#!/bin/sh

# ssmtp
# sed -i 's/^mailhub=.*$/mailhub=mailhog:25\n/ ; s/^hostname.*$/hostname=dcc.local\n/' /etc/ssmtp/ssmtp.conf

echo "define(\`SMART_HOST', \`mailhog:25')dnl" >> /etc/mail/sendmail.mc
make -C /etc/mail

exec /usr/sbin/sendmail -bvD