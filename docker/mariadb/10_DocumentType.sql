-- MariaDB dump 10.19  Distrib 10.4.22-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: dcc_docdb
-- ------------------------------------------------------
-- Server version	10.4.22-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `DocumentType`
--

DROP TABLE IF EXISTS `DocumentType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DocumentType` (
  `DocTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `ShortType` varchar(32) DEFAULT NULL,
  `LongType` varchar(255) DEFAULT NULL,
  `TimeStamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `NextDocNumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`DocTypeID`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DocumentType`
--

LOCK TABLES `DocumentType` WRITE;
/*!40000 ALTER TABLE `DocumentType` DISABLE KEYS */;
INSERT INTO `DocumentType` VALUES
(1,'C - Contractual or procurement','Contractual or procurement related documents such as RFPs, source selection reports, SOWs, invoices, and all correspondence with contractors','2022-02-04 18:46:11',22),
(2,'D - Drawings','Engineering Drawings, CAD outputs','2022-01-30 18:57:23',31),
(4,'E - Engineering documents','Engineering documents such as material lists, specifications, documents submitted to internal PDRs, parts lists, DCNs and process travelers','2022-02-04 19:52:31',47),
(11,'F - Forms and Templates','Forms and Templates','2021-12-19 19:26:57',20),
(5,'G - Presentations (eg Graphics)','Viewgraphs for presentations, photos, multimedia, sketches, etc','2022-02-03 19:34:49',148),
(6,'L - Letters and Memos','General correspondence (letters, memos, faxes) that do not belong in another category','2022-02-03 05:08:51',11),
(7,'M - Management or Policy','Policy or management documents such as the Project Management Plan or Publication Policy','2022-02-03 20:52:49',27),
(8,'P - Publications','Formal physics or technical notes intended for publication and reviewed by a committee; student theses','2022-02-04 09:59:49',37),
(9,'S - Serial numbers','Serial Numbers for LIGO Equipment','2022-01-21 16:57:22',77),
(10,'T - Technical notes','Technical notes used internally or released but not intended for publication and not considered reviewed','2022-02-04 15:14:34',46),
(12,'Q - Quality Assurance documents','Quality Assurance documents','2021-11-05 22:00:27',39),
(13,'X - Safety Incident Reports','Safety Incident Reports','2022-02-03 17:43:29',5),
(14,'R - Operations Change Requests','Operations Change Order Requests','2022-02-02 21:46:49',7),
(15,'A - Acquisitions','Property Acquisitions','2022-02-03 20:41:52',13),
(16,'I - Internal to LIGOLab','Internal Documents to LIGOLab (engineering information / documentation)','2022-02-04 18:10:42',1);
/*!40000 ALTER TABLE `DocumentType` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-04 13:01:42
