FROM debian:bookworm as base

ARG PERL_TEST_OPTION="--notest"
ARG MARIADB_ROOT_PASSWORD=changeme
ARG MARIADB_DATABASE=dcc_docdb
ARG MARIADB_USER=docdbrw
ARG MARIADB_PASSWORD
ARG MARIADB_USER2=docdbro
ARG MARIADB_PASSWORD2
ARG LDAP_BASE_DN="dc=ligo,dc=org"
ARG LDAP_SERVER="ldap-server"
ARG LDAP_ADMIN_PASSWORD='test1234' \
    LDAP_ORGANISATION=''  \
    LDAP_DOMAIN=""  \
    LDAP_SERVER=""

ENV DOCDB_BASE=/usr1/www
ENV DOCDB_CGI_DIR=${DOCDB_BASE}/cgi-bin
ENV DOCDB_HTML_DIR=${DOCDB_BASE}/html
ENV DEBIAN_FRONTEND=noninteractive
ENV PERL5LIB=/usr1/www/perl5

ENV MARIADB_ROOT_PASSWORD=${MARIADB_ROOT_PASSWORD}
ENV MARIADB_DATABASE=${MARIADB_DATABASE}
ENV MARIADB_USER=${MARIADB_USER}
ENV MARIADB_PASSWORD=${MARIADB_PASSWORD}
ENV MARIADB_USER2=${MARIADB_USER2}
ENV MARIADB_PASSWORD2=${MARIADB_PASSWORD2}

SHELL ["/bin/bash", "-l", "-c"]

# probably can skip default-libmysqlclient-dev libmariadb-dev-compat libexpat1-dev
RUN apt update && apt upgrade -y &&\
    apt install -y apache2 apache2-utils supervisor build-essential \
                   libapache2-mod-authnz-external libapache2-mod-auth-openidc \
                   mariadb-client redis tmux vim curl ksh tcl openssl j2cli \
                   libdbi-perl libdbd-mariadb-perl libdbd-mysql-perl libdbd-sqlite3-perl \
                   libdbd-ldap-perl libcam-pdf-perl libcgi-pm-perl libcgi-untaint-perl \
                   libconfig-tiny-perl libemail-simple-perl libemail-sender-perl libemail-sender-perl \
                   libfont-ttf-perl libhtml-parser-perl libio-string-perl libparams-validate-perl \
                   libtext-pdf-perl libdatetime-perl libdigest-sha-perl\
                   libxml-grove-perl libxml-grove-perl libxml-perl libxml-twig-perl &&\
    mkdir -p ${DOCDB_CGI_DIR} ${DOCDB_HTML_DIR} ${PERL5LIB}


#RUN apt install -y libcgi-untaint-perl libxml-libxml-perl cpanminus j2cli
RUN apt install -y cpanminus
COPY docker/cpanfile ${DOCDB_BASE}
RUN  cd ${DOCDB_BASE} && cpanm --cpanfile cpanfile  --build-args="-j8" --installdeps ${PERL_TEST_OPTION} .


RUN apt-get clean

RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
                -subj  "/C=US/ST=CA/O=LIGO@caltech/CN=dcc.local." \
                -keyout /etc/ssl/private/apache-selfsigned.key \
                -out /etc/ssl/certs/apache-selfsigned.crt

#FIXME: full apache configuration
COPY docker/docdb.j2 /tmp/
COPY apache-configurations/dcc/conf.d/short_url.conf /etc/apache2/conf-available
RUN chown -R www-data. ${DOCDB_HTML_DIR}/ &&\
    j2 /tmp/docdb.j2 > /etc/apache2/sites-available/docdb.conf &&\
    a2ensite docdb.conf && \
    a2enconf short_url.conf &&\
    a2dissite 000-default && \
    a2enmod cgid proxy proxy_http rewrite ssl authnz_external include && \
    a2enmod ldap authnz_ldap &&\
    cat /etc/apache2/sites-available/docdb.conf  && \
    apache2ctl -t

# create user base
COPY docker/DCCTesters /tmp
RUN htpasswd -cb /etc/apache2/passwords admin admin && \
    xargs -a /tmp/DCCTesters -t -n 2 htpasswd -b /etc/apache2/passwords &&\
    chown www-data:www-data /etc/apache2/passwords &&\
    chmod 0660 /etc/apache2/passwords


# RUN echo "export PATH=\"$PATH\"">> /etc/apache2/envvars
# COPY www/html ${DOCDB_HTML_DIR}

#FIXME: public website does not work as expected
#FIXME: hypothesis: PERL5LIB forces to load the wrong SiteConfig.pm
# copy the private site code on the public site location
# COPY www/cgi-bin/private/DocDB/* ${DOCDB_CGI_DIR}/DocDB/
# copy the private site code on the the private site location
# AND overwrite the public site location with public site specific code
# COPY www/cgi-bin ${DOCDB_CGI_DIR}
# Create a few empty dirs as needed (/public/Static is probably not needed here)
RUN mkdir -p ${DOCDB_HTML_DIR}/DocDB/0000    \
             ${DOCDB_HTML_DIR}/public/0000

# not covered by cmake deployment below
COPY --chown=www-data www/html/public/Static  ${DOCDB_HTML_DIR}/public/Static/
# COPY --chown=www-data www/html/login ${DOCDB_HTML_DIR}/login/

COPY www /tmp/www/
COPY deployment /tmp/deployment

RUN apt-get install -y cmake
COPY www/ /tmp/project/www
COPY cmake/ /tmp/project/cmake
COPY etc/ /tmp/project/etc
COPY docs/ /tmp/project/docs
COPY CMakeLists.txt /tmp/project/CMakeLists.txt
RUN mkdir -p /tmp/build  && cd /tmp/build && \
    cmake -DCMAKE_INSTALL_PREFIX=${DOCDB_BASE}  -DBUILD_WITH_DOCS=  /tmp/project/

# push from /tmp/prject to CMAKE_INSTALL_PREFIX, aka DOCDB_BASE
RUN cd /tmp/build && \
    cmake --build . --target push_changed_files



# set sendmail to use mailhog
# defer the config or the service to runtime because it needs to known the hostnamed
# also https://metacpan.org/pod/Email::Sender::Manual::QuickStart#specifying-transport-in-the-environment
ENV EMAIL_SENDER_TRANSPORT=SMTP
ENV EMAIL_SENDER_TRANSPORT_host=mailhog
ENV EMAIL_SENDER_TRANSPORT_port=25


#gimme that template
COPY docker/SiteConfig.j2 /tmp
#FIXME: public siteconfig.pm probably needs its own j2 templates
RUN j2 /tmp/SiteConfig.j2 > ${DOCDB_CGI_DIR}/private/DocDB/SiteConfig.pm
RUN j2 /tmp/SiteConfig.j2 | sed 's!/private/!/!' >  ${DOCDB_CGI_DIR}/DocDB/SiteConfig.pm

RUN cp ${DOCDB_HTML_DIR}/Static/css/DocDB.css ${DOCDB_HTML_DIR}/Static/css/LIGODocDB.css
RUN cp ${DOCDB_HTML_DIR}/public/Static/css/DocDB.css ${DOCDB_HTML_DIR}/public/Static/css/LIGODocDB.css


#FIXME: needs glimpse
RUN apt install -y glimpse

COPY docker/supervisord.conf /etc/supervisord.conf


# CGI removed from perl core at 5.22: startform -> start_form, endform -> end_form
# https://github.com/ericvaandering/DocDB/issues/12
RUN  find ${DOCDB_CGI_DIR}/DocDB ${DOCDB_CGI_DIR}/private/DocDB -type f | xargs sed -i 's/startform/start_form/; s/endform/end_form/' && \
     chown -R www-data ${DOCDB_HTML_DIR}
# '.' removed from @INC at 5.26(?). explicitely adding '.' to the lib path
RUN  find ${DOCDB_CGI_DIR}/DocDB ${DOCDB_CGI_DIR}/private/DocDB -type f | xargs sed -i '0,/^require/{s//use lib qw(.);\nrequire/}'
# 2022-03-17 seems to work fine. However I (PGA) prefer moving *.pm files to PERL5LIB dir
# Caveat: above is seamless for public site's SiteConfig.pm below would require different PERL5LIB in public site context.

# or move the *.pm files to $PERL5LIB ?
#RUN mv ${DOCDB_CGI_DIR}/private/DocDB/*.pm ${PERL5LIB}/ &&\
#    mv ${DOCDB_CGI_DIR}/private/DocDB/Params ${PERL5LIB}/ &&\
#    rm ${DOCDB_CGI_DIR}/DocDB/*.pm -f

# for use at runtime..
COPY cypress/cypress/fixtures/users.json /fixtures/users.json
COPY docker/DCCTesters.j2 /fixtures
COPY docker/DCCTesters.sql.j2 /fixtures

COPY docker/supervisord.d/*.conf /etc/supervisord.d/

# apache, redis
EXPOSE 80 443 6379
# supervisord
EXPOSE 9001

CMD [ "/usr/bin/supervisord" ]
