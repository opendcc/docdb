# The DocDB Documentation

## How to Syncronize Between Systems

### Setup Phase

Before any of the following actions be done,
the system needs to be configured.
In CMake, options are passed with the -D option.

 <table>
  <tr>
    <th>Option</th>
    <th>Type</th>
    <th>Description</th>
  </tr>
  <tr>
    <td>CMAKE_INSTALL_PREFIX</td>
    <td>PATH</td>
    <td>Install directory used by install().</td>
  </tr>
  <tr>
    <td>CMAKE_INSTALL_SYSCONFDIR</td>
    <td>PATH</td>
    <td>System configuration directory (ex: /etc).</td>
  </tr>
</table>

The general syntax is

    cmake [<option> ..] <top_source_directory>

An example configuration command is given below

    cmake -DCMAKE_INSTALL_PREFIX=/usr1/www /home/ed.maros/mnt/Sources/LIGO_GC/docdb

### Get help on each build target

    cmake --build . --target guide

### Query Which Files Have Changed

    cmake --build . --target list_changed_files

### Get a diff between the working files and the files in cmake install prefix location

    cmake --build . --target diff_changed_files

### Pull Changes Into Your Local Repository

This action will pull from the CMAKE_INSTALL_PREFIX
into the currently checked out branch.

    cmake --build . --target pull_changed_files

### Push Changes From Your Local Repository To The CMake Install Prefix

This action will push changes from the currently checked out
to the CMAKE_INSTALL_PREFIX.
Caution should be used when performing this operation as
it can modify the production system.

    cmake --build . --target push_changed_files
    
### Create a branch to capture what is on a host

    #
    # Create a temporary snapshot of host
    # - mount the docdb sources on remote system
    # - change to the main branch
    # - create host branch from main
    # - capture the differences
    # - commit changes to the host branch for further use
    #
    ssh user@host
    sshfs -o allow_other user@development.host:Source/docdb ~/mnt/docdb
    cd ~/mnt/docdb
    git checkout main
    git checkout -b host
    cd ~/build/docdb
    cmake -DCMAKE_INSTALL_PREFIX=/usr1/www -DCMAKE_INSTALL_SYSCONFDIR=/etc ~/mnt/docdb
    cmake --build .
    cmake --build . --target pull_changed_files
    git commit -m 'snapshot of host' -a
    exit

### Find out if there are any uncommitted changes on a system installation

This is useful to find out if there are changes on a remote system
that have not been committed to the repository.

    ssh user@host
    sshfs -o allow_other user@development.host:Source/docdb ~/mnt/docdb
    cd ~/mnt/docdb
    git checkout main
    git checkout -b host
    cd ~/build/docdb
    cmake -DCMAKE_INSTALL_PREFIX=/usr1/www -DCMAKE_INSTALL_SYSCONFDIR=/etc ~/mnt/docdb
    cmake --build .
    cmake --build . --target pull_changed_files
    git commit -m 'snapshot of host' -a
    exit
    git fetch upstream
    git checkout main
    git rebase upstream/main
    git diff main..host

### Compare Differences Between 2 Machines

    #
    # Create a temporary snapshot of host1
    #
    ssh user@host1
    sshfs -o allow_other user@development.host1:Source/docdb ~/mnt/docdb
    cd ~/mnt/docdb
    git checkout main
    git checkout -b host1
    cd ~/build/docdb
    cmake -DCMAKE_INSTALL_PREFIX=/usr1/www -DCMAKE_INSTALL_SYSCONFDIR=/etc ~/mnt/docdb
    cmake --build .
    cmake --build . --target pull_changed_files
    git commit -m 'snapshot of host1' -a
    exit
    #
    # Create a temporary snapshot of host2
    #
    ssh user@host2
    sshfs -o allow_other user@development.host2:Source/docdb ~/mnt/docdb
    cd ~/mnt/docdb
    git checkout main
    git checkout -b host2
    cd ~/build/docdb
    cmake -DCMAKE_INSTALL_PREFIX=/usr1/www -DCMAKE_INSTALL_SYSCONFDIR=/etc ~/mnt/docdb
    cmake --build .
    cmake --build . --target pull_changed_files
    git commit -m 'snapshot of host2' -a
    exit
    #
    # Look at the differences between the two systems
    #
    git diff host2..host1
    #
    # When all work is done, do cleanup by unconditionally removing
    #  the temporary branches that were created for the sole purpose
    #  of being able to use GIT to show the differences
    #
    git branch -D host1
    git branch -D host2
