query to return the list of DocID of documents made public.

```sql
select doc.Alias, rev.VersionNumber 
    from DocumentRevision as rev 
    join Document as doc 
        on doc.DocumentID=rev.DocumentID 
    left join RevisionSecurity as sec  
        on rev.DocRevID = sec.DocRevID 
    where rev.Obsolete != 1 and sec.DocRevID is null 
    order by doc.DocumentID asc;
```



