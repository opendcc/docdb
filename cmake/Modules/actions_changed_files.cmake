# -----------------------------------------------------------------------
# -----------------------------------------------------------------------

function( display_guide_text )
  set( options
    )
  set( oneValueArgs
    TARGET
    USAGE
    )
  set( multiValueArgs
    TEXT
    )

  cmake_parse_arguments( ARG
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN}
    )

  if ( TARGET ${ARG_TARGET} )
    return( )
  endif( )
  if ( NOT TARGET guide )
    add_custom_target( guide )
  endif( )

  set( prefix "${CMAKE_COMMAND}" "-E" "echo" )
  foreach( line ${ARG_TEXT} )
    list( APPEND command ${prefix} "${line}" )
    set( prefix "COMMAND" "${CMAKE_COMMAND}" "-E" "echo" )
  endforeach( )
  if ( ARG_USAGE )
    list( APPEND command ${prefix} " " )
    list( APPEND command ${prefix} "  USAGE: ${ARG_USAGE}" )
    list( APPEND command ${prefix} " " )
  endif( )

  add_custom_target( ${ARG_TARGET} ${command} )
  add_dependencies( guide ${ARG_TARGET} )
endfunction( )

include( list_changed_files )
include( diff_changed_files )
include( pull_changed_files )
include( push_changed_files )

function( actions_changed_files )
  list_changed_files( ${ARGN} )
  diff_changed_files( ${ARGN} )
  pull_changed_files( ${ARGN} )
  push_changed_files( ${ARGN} )
endfunction( )
