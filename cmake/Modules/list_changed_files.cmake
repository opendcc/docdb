# -----------------------------------------------------------------------
# -----------------------------------------------------------------------

display_guide_text(
  TARGET guide_list_changed_files
  USAGE "  ${CMAKE_COMMAND} --build . --target list_changed_files"
  TEXT
  " "
  "target: list_changed_files"
  " "
  "  This target lists the files that are differnt between"
  "  the source directory and the destination directory."
  )

if ( NOT TARGET list_changed_files )
  add_custom_target( list_changed_files )
endif( )

function( list_changed_files )
  set( options
    )
  set( oneValueArgs
    GIT_PREFIX
    INSTALLED_PREFIX
    TARGET_PREFIX
    USER
    GROUP
    )
  set( multiValueArgs
    FILES
    PERMISSIONS
    )

  cmake_parse_arguments( ARG
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN}
    )

  set( target ${ARG_TARGET_PREFIX}_list_changed_files )
  add_custom_target(
    ${target}
    ${CMAKE_COMMAND}
    -DMODE=list
    "-DGIT_PREFIX=${ARG_GIT_PREFIX}"
    "-DINSTALLED_PREFIX=${ARG_INSTALLED_PREFIX}"
    "-DFILES='${ARG_FILES}'"
    -P ${PROGRAM_CHANGED_FILES}
    )
  add_dependencies( list_changed_files ${target} )
endfunction( )
