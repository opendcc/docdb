# -----------------------------------------------------------------------
# -----------------------------------------------------------------------

display_guide_text(
  TARGET guide_diff_changed_files
  USAGE "  ${CMAKE_COMMAND} --build . --target diff_changed_files"
  TEXT
  " "
  "target: diff_changed_files"
  " "
  "  This target shows the content differences of the files in"
  "  the source directory verses the destination directory."
  )

if ( NOT TARGET diff_changed_files )
  add_custom_target( diff_changed_files )
endif( )
if ( NOT PROGRAM_DIFF )
  find_program( PROGRAM_DIFF diff )
endif( )

function( diff_changed_files )
  set( options
    )
  set( oneValueArgs
    GIT_PREFIX
    INSTALLED_PREFIX
    TARGET_PREFIX
    )
  set( multiValueArgs
    FILES
    )

  cmake_parse_arguments( ARG
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN}
    )

  set( target ${ARG_TARGET_PREFIX}_diff_changed_files )
  add_custom_target(
    ${target}
    ${CMAKE_COMMAND}
    -DMODE=diff
    -DPROGRAM=${PROGRAM_DIFF}
    "-DGIT_PREFIX=${ARG_GIT_PREFIX}"
    "-DINSTALLED_PREFIX=${ARG_INSTALLED_PREFIX}"
    "-DFILES='${ARG_FILES}'"
    -P ${PROGRAM_CHANGED_FILES}
    )
  add_dependencies( diff_changed_files ${target} )
endfunction( )
