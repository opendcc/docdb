# -----------------------------------------------------------------------
# -----------------------------------------------------------------------

display_guide_text(
  TARGET guide_pull_changed_files
  USAGE "  ${CMAKE_COMMAND} --build . --target pull_changed_files"
  TEXT
    " "
    "target: pull_changed_files"
    " "
    "  This target pulls changed files from the installation"
    "  directory into the source directory."
  )

if ( NOT TARGET pull_changed_files )
  add_custom_target( pull_changed_files )
endif( )
if ( NOT PROGRAM_DIFF )
  find_program( PROGRAM_DIFF diff )
endif( )

function( pull_changed_files )
  set( options
    )
  set( oneValueArgs
    GIT_PREFIX
    INSTALLED_PREFIX
    TARGET_PREFIX
    )
  set( multiValueArgs
    FILES
    )

  cmake_parse_arguments( ARG
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN}
    )

  set( target ${ARG_TARGET_PREFIX}_pull_changed_files )
  add_custom_target(
    ${target}
    ${CMAKE_COMMAND}
    -DMODE=pull
    "-DGIT_PREFIX=${ARG_GIT_PREFIX}"
    "-DINSTALLED_PREFIX=${ARG_INSTALLED_PREFIX}"
    "-DFILES='${ARG_FILES}'"
    -P ${PROGRAM_CHANGED_FILES}
    )
  add_dependencies( pull_changed_files ${target} )
endfunction( )
