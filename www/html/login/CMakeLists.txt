# -----------------------------------------------------------------------
# -----------------------------------------------------------------------

# add subdirs
#Documentation_files
#FAQ_files
#Getting_DCC_Help_files
#Quick_Start_Guide_files

add_subdirectory( css )
add_subdirectory( js )
add_subdirectory( images )




set( login_files
    BigRedButton.shtml
    css
    Documentation.html
    FAQ.html
    footer.shtml
    Getting_DCC_Help.html
    header-portal.shtml
    header.shtml
    idpselect_config.js
    idpselect.css
    idpselect.js
    index.shtml
    news.shtml
    Quick_Start_Guide.html
    typeahead.js
  )


set( login_dir ${CMAKE_INSTALL_PREFIX}/html/login )

actions_changed_files(
  TARGET_PREFIX login_htlm
  GIT_PREFIX ${CMAKE_CURRENT_SOURCE_DIR}
  INSTALLED_PREFIX ${login_dir}
  FILES ${login_files}
  )
