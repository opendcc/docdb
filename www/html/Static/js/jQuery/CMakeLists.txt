# -----------------------------------------------------------------------
# -----------------------------------------------------------------------

set( jquery_files
  jquery-1.8.js
  jquery-ui.js
  )

set( jquery_dir ${CMAKE_INSTALL_PREFIX}/html/Static/js/jQuery )

actions_changed_files(
  TARGET_PREFIX jquery_js_static_html
  GIT_PREFIX ${CMAKE_CURRENT_SOURCE_DIR}
  INSTALLED_PREFIX ${jquery_dir}
  FILES ${jquery_files}
  )

