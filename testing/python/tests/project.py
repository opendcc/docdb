"""
This is a file intended to specify project configuration values
"""
DEFAULT_BATCH_USER = "admin"
DCC_CONFIGURATION_DEFAULT = "/e2e/playwright/dcc_configuration"
PROJECT_URL = "https://dcc"
