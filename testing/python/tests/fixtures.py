import pytest
import pytest_asyncio
import nest_asyncio

from playwright.async_api import async_playwright

from models.igwn.browser import AsyncBrowser
from models.igwn.request_wrapper import request_wrapper
from models.igwn.User import User
from models.igwn.Users import Users

from models.dcc.Document import Document
from models.dcc.Utilities import create_document

nest_asyncio.apply( )


@pytest.fixture(scope="session")
def global_initialize() -> None:
    # looks like sync api does not need the global_initialize.
    # I'm keeping it anyway for now, to have the option to plug stuff
    # e.g. reporter config (local files on my laptop, testmo in CI)
    return None


@pytest.fixture(autouse=True, scope="session")
def browser_context_args(browser_context_args):
    return {**browser_context_args, "ignore_https_errors": True}


@pytest.fixture(autouse=True, scope="session")
def dcc_setup_teardown():
    global users

    # Setup
    if ( ( 'users' in globals() ) and ( not users == None ) ):
        if users[next(iter(users))]["password"] == "-":
            users[next(iter(users))]["password"] = getpass.getpass()
    # Call test functions
    yield
    # Teardown


@pytest.fixture(autouse=True, scope="function")
def random_document(base_url: str):  # base url is itself a fixture
    doc = Document( )
    create_document( doc, base_url )
    return doc


@pytest.fixture(autouse=True, scope="session")
def file_to_upload():
    return "tests/assets/ligologo_t.png"

@pytest_asyncio.fixture(scope="function")
async def async_page(browser_context_args, request ):
    request_info = request_wrapper( request )
    browser_name = 'chromium'
    async with async_playwright( ) as playwright:
        browser = AsyncBrowser(playwright, browser_name, browser_context_args, request_info)
        await browser.launch_browser()
        yield browser.page
        # Get the test status (passed or failed)
        #status = "failed" if request.node.wasxfail else "passed"
        status = "passed"
        await browser.close_browser(status)
