import re
from models.PageWithTitleModel import PageWithTitleModel


class DCC(PageWithTitleModel):
    def __init__(self, page):
        super().__init__(page, "/dcc", "Document Control Center")
        self.true_url = "/cgi-bin/private/DocDB/DocumentDatabase"
        self.true_url_pattern = re.compile(r"^.*" + self.true_url + r"$")
        # Buttons
        self.author_button = page.locator('input[value="Author"]')
        self.title_button = page.locator('input[value="Title"]')
        self.identifier_button = page.locator('input[value="Identifier"]')
        self.changes_button = page.locator('input[value="Changes"]')
        # self.these_words_button = page.locator( 'input[value="These Words"]')
        self.these_words_button = page.get_by_text("These Words")
        self.advanced_search_button = page.locator('input[value="Advanced search"]')
        self.authors_button = page.locator('input[value="Authors"]')
        self.topics_button = page.locator('input[value="Topics"]')
        self.access_groups_button = page.locator('input[value="Access groups"]')
        self.events_by_type_button = page.locator('input[value="Events by type"]')
        self.events_by_date_button = page.locator('input[value="Events by date"]')
        self.signature_report_button = page.locator('input[value="Signature Report"]')
        # Text Fields

    def is_on_page(self):
        return self.true_url_pattern.search(self.page.url) or super().is_on_page()
