import os
import re
import urllib.parse
from typing import Union

from models.igwn.Utilities import is_batch

from models.igwn.User import User
from models.igwn.Users import Users

from models.igwn.PageBase import PageTypes, SyncPage, AsyncPage
from models.igwn.LoginBase import LoginBase

class Login( LoginBase ):
    def __init__(self, page: PageTypes, baseUrl: str ):
        super().__init__(page, baseUrl)

    def _sync_login(self, user: Union[str, User]):
        user_info = self._get_user_info( user )
        if is_batch():
            url = self._login_batch_url( user_info )
            self.page.goto(url)
            self.page.wait_for_load_state()
        else:
            self.navigate()
            if re.match(r".*\/login\/.*", self.page.url):
                self.page.locator("#loginbutton").click()
                self.page.wait_for_load_state()
                self.page.locator("[alt=LIGO]").click()
                self.page.wait_for_load_state()
                #
                # Fill in the login form
                #
                self.page.locator("#username").fill(user_info.username)
                self.page.locator("#password").fill(user_info.password)
                self.page.locator("[name=_eventId_proceed]").click()
                self.page.wait_for_load_state()

    async def _async_login(self, user: Union[str, User]):
        user_info = self._get_user_info( user )
        if is_batch():
            url = self._login_batch_url( user_info )
            await self.page.goto(url)
            await self.page.wait_for_load_state()
        else:
            self.navigate()
            if re.match(r".*\/login\/.*", self.page.url):
                self.page.locator("#loginbutton").click()
                self.page.wait_for_load_state()
                self.page.locator("[alt=LIGO]").click()
                self.page.wait_for_load_state()
                #
                # Fill in the login form
                #
                self.page.locator("#username").fill(user_info.username)
                self.page.locator("#password").fill(user_info.password)
                self.page.locator("[name=_eventId_proceed]").click()
                self.page.wait_for_load_state()

    def _get_user_info(self, user: Union[str, User]) -> User:
        user_info = None;

        if ( isinstance( user, str ) ):
            user_info = Users.get( user )
        elif ( isinstance( user, User ) ):
            user_info = user
        return user_info

    def _login_batch_url(self, user: User) -> str:
        url = ""
        url_encoded_username = urllib.parse.quote(user.username)
        if ( user.password ):
            url_encoded_password = urllib.parse.quote(user.password)
            url = self.base_url.replace(
                "https://",
                "https://{}:{}@".format(url_encoded_username, url_encoded_password),
            )
        else:
            url = self.base_url.replace(
                "https://",
                "https://{}@".format(url_encoded_username),
            )
        return url

