from models.PageWithTitleModel import PageWithTitleModel


class DocumentAddForm(PageWithTitleModel):
    def __init__(self, page):
        super().__init__(
            page, "/cgi-bin/private/DocDB/DocumentAddForm", "Document Addition"
        )
        # fixup sync/async methods to have a uniform api as much as possible
        if self.model_is_async:
            self.navigate = self._async_navigate
            self.change_metadata = self._async_change_metadata
            self.navigate_to_upload_version = self._async_navigate_to_upload_version
        else:
            self.navigate = self._sync_navigate
            self.change_metadata = self._sync_change_metadata
            self.navigate_to_upload_version = self._sync_navigate_to_upload_version

        self.title = page.locator('input[name="title"]')
        self.old_dcc_number = page.locator('input[name="oldnumber"]')
        self.abstract = page.locator('textarea[name="abstract"]')
        self.keywords = page.locator('input[name="keywords"]')
        self.doctype = page.get_by_label("Technical notes")

        self.view_groups = page.locator('select[name="security"]')
        self.view_hierarchical = self.view_groups.first
        self.view_specific = self.view_groups.last
        self.modifiable_groups = page.locator("select[name=modify]")
        self.modify_hierarchical = self.modifiable_groups.first
        self.modify_specific = self.modifiable_groups.last
        self.authors = page.locator("#authormanual")
        self.topics = page.locator("select[name=topics]")
        # <textarea name="revisionnote" rows="6" cols="60"></textarea>
        self.revision_note = page.locator('textarea[name="revisionnote"]')
        self.local_file_upload_block = page.get_by_text("Local file upload")

        # <select> for number of upload slots name="maxfiles"
        # table id="dynamicUploadSlots" ; number of rows in the table
        #       change when selecting different values above

        self.upload1_file_chooser = page.locator("#upload1")
        self.filedesc1_input = page.locator("input[name=filedesc1]")
        self.main1_checkbox = page.locator("input[name=main1]")

        self.signature_text_area = page.locator("#signofflist")
        self.parallel_signoff_checkbox = page.locator("input[name=parallelsignoff]")

        self.reserve_buttons = page.locator('input[value="Request document ID"]')
        self.use_dcc_number_buttons = page.locator('input[value="Use DCC Number"]')
        self.submit_document_buttons = page.locator('input[value="Submit document"]')
        self.upload_new_version_buttons = page.locator(
            'input[value="Upload Document/Add New Revision"]'
        )
        self.change_metadata_button = page.get_by_text("Change Metadata")

        self.successful_message = page.get_by_text(
            "You were successful. Your Document ID is"
        )
        self.view_document_button = self.page.get_by_text("View Document")
        self.error_message = self.page.locator("dl.error")

        # TODO: more widgets whose presence to assert on the various presentaiton of this page:
        # - all: signature,events
        # - add: file upload (with dynamic change number of files)

    def _sync_navigate(self, mode="add"):
        """
        darnit: that page type is actually polymorph depending on the mode parameter
        :param mode: add | use | reserve
        :return:
        """
        self.page.goto(f"{self.base_url}?mode={mode}")

    async def _async_navigate(self, mode="add"):
        """
        darnit: that page type is actually polymorph depending on the mode parameter
        :param mode: add | use | reserve
        :return:
        """
        await self.page.goto(f"{self.base_url}?mode={mode}")

    def _sync_navigate_to_upload_version(self, doc_alias: str):
        # you really do not want to know why...
        self.page.goto(f"/{doc_alias}")
        self.upload_successful_message = self.page.get_by_text(
            f"You were successful. Your Document ID is LIGO-{doc_alias}, version 1."
        )
        self.page.get_by_text("Upload LIGO-").click()

    async def _async_navigate_to_upload_version(self, doc_alias: str):
        # you really do not want to know why...
        await self.page.goto(f"/{doc_alias}")
        self.upload_successful_message = self.page.get_by_text(
            f"You were successful. Your Document ID is LIGO-{doc_alias}, version 1."
        )
        await self.page.get_by_text("Upload LIGO-").click()

    def _sync_change_metadata(self, doc_alias: str):
        # you really do not want to know why...
        self.page.goto(f"/{doc_alias}")
        # <input type="submit" name=".submit" value="Change Metadata">
        self.page.get_by_text("Change Metadata").click()

    async def _async_change_metadata(self, doc_alias: str):
        # you really do not want to know why...
        await self.page.goto(f"/{doc_alias}")
        # <input type="submit" name=".submit" value="Change Metadata">
        await self.page.get_by_text("Change Metadata").click()
