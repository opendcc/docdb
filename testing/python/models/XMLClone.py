from models.PageWithTitleModel import PageWithTitleModel
import re
import pytest


class XMLClone(PageWithTitleModel):
    """
    """
    def __init__(self, page):
        super().__init__(page, "/cgi-bin/private/DocDB/XMLClone", "Clone Doucment Reservation Results")
        # fixup sync/async methods to have a uniform api as much as possible
        if self.model_is_async:
            pass
        else:
            pass

        self.page = page
        self.successful = page.get_by_text(
            "You were successful"
        )

        self.goto_new_document_button = page.get_by_role( 'button', name = 'Go To New Document' )
        self.goto_original_document_button = page.get_by_role( 'button', name = 'Go To Original Document' )
