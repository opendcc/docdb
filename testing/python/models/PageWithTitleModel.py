from models.igwn.PageModel import PageModel

class PageWithTitleModel(PageModel):
    def __init__(self, page, baseUrl: str, title: str):
        super().__init__(page, baseUrl)
        self.title = page.locator("h1#title", has_text=title)
        # Title Bar Elements
        self.home = page.locator(":nth-match(:text('Home'), 1)")
        self.reserve_number = page.locator(":nth-match(:text('Reserve Number'), 1)")
        self.search = page.locator(":nth-match(:text('Search'), 1)")
        self.recent_changes = page.locator(":nth-match(:text('Recent Changes'), 1)")
        self.topics = page.locator(":nth-match(:text('Topics'), 1)")
        self.events = page.locator(":nth-match(:text('Events'), 1)")
        self.public = page.locator(":nth-match(:text('Public'), 1)")
        self.assist = page.locator(":nth-match(:text('Help'), 1)")
