from models.PageWithTitleModel import PageWithTitleModel

class MeetingsModify( PageWithTitleModel ):
    """
    """

    def __init__(self, page):
        super().__init__( page, "/cgi-bin/private/DocDB/MeetingModify", "Create a New Event" )
        self.button_modify_event_and_sessions = \
            page.get_by_role( 'button', name='Modify Event and Sessions')
        self.description_created_event = \
            page.locator( 'dd:has-text("Created new event:")' )
        self.description_created_session = page.locator( 'dd:has-text("Created session:")' )
        self.description_modified_event = \
            page.locator( 'dd:has-text("Modified event:")' )
        self.popup_user_suggestions = \
            page.locator('li[role=menuitem]')
        self.day_end = page.locator('select[name=endday]')
        self.day_start = page.locator('select[name=startday]')
        self.description_long = page.locator('input[name=long]')
        self.description_short = page.locator('input[name=shortdesc]')
        self.event_groups = page.locator('select[name=eventgroups]')
        self.list_of_moderators = page.locator('textarea#list_moderators')
        self.list_of_moderators_n1 = page.locator('textarea#list_moderators-n1')
        self.session_title = page.locator('input[name=sessiontitle]').first
