from models.PageWithTitleModel import PageWithTitleModel


class RecentChanges(PageWithTitleModel):
    def __init__(self, page):
        super().__init__(
            page, "/cgi-bin/private/DocDB/ListBy", "Documents from Last 2 Days"
        )
