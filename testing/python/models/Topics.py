from models.PageWithTitleModel import PageWithTitleModel


class Topics(PageWithTitleModel):
    def __init__(self, page):
        super().__init__(page, "/cgi-bin/private/DocDB/ListTopics", "List of Topics")
