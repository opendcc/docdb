from models.PageWithTitleModel import PageWithTitleModel
import pytest


class ListBy( PageWithTitleModel ):
    """
    """

    def __init__(self, page):
        super().__init__( page, "/cgi-bin/private/DocDB/ListBy", "Document List by Event Group" )

    def button_ligo_number( self, ligo_number ):
        locator_text=f'table.LowPaddedTable > tbody > tr > td:has-text("{ligo_number}")'
        return self.page.locator( locator_text )
