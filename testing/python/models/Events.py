from models.PageWithTitleModel import PageWithTitleModel


class Events(PageWithTitleModel):
    def __init__(self, page):
        super().__init__(
            page, "/cgi-bin/private/DocDB/ListAllMeetings", "List of Events"
        )
