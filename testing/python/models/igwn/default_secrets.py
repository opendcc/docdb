users = {
    "admin": {
        "username": "admin",
        # Password to use to log in user.
        # If the value is '-', then the user is prompted
        #   at runtime for the password which is then stored
        #   internally for the session.
        "password": "admin",
        "lscname": "admin",
        "sir_name": "Administer",
    }
}
