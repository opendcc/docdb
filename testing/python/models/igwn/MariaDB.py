import os
import mysql.connector

from models.igwn.DatabaseHandler import DatabaseHandler

class MariaDB(DatabaseHandler):
    def __init__(self, host = None, user = None, password = None, database = None):
        if ( host is None ):
            host = os.getenv( 'MARIADB_HOST' )
        if ( user is None ):
            user = os.getenv( 'MARIADB_USER' )
        if ( password is None ):
            password = os.getenv( 'MARIADB_PASSWORD' )
        if ( database is None ):
            database = os.getenv( 'MARIADB_DATABASE' )
        self.host = host
        self.user = user
        self.password = password
        self.database = database
        self.conn = None

    def connect(self):
        try:
            self.conn = mysql.connector.connect(
                host=self.host,
                user=self.user,
                password=self.password,
                database=self.database
            )
            print("Connected to MariaDB database successfully.")
        except mysql.connector.Error as e:
            print("Error connecting to MariaDB database:", e)

    def execute_query(self, query, params=None):
        try:
            cursor = self.conn.cursor()
            if params:
                cursor.execute(query, params)
            else:
                cursor.execute(query)
            self.conn.commit()
            print("Query executed successfully.")
        except mysql.connector.Error as e:
            print("Error executing query:", e)

    def fetch_data(self, query, params=None):
        try:
            cursor = self.conn.cursor()
            if params:
                cursor.execute(query, params)
            else:
                cursor.execute(query)
            rows = cursor.fetchall()
            return rows
        except mysql.connector.Error as e:
            print("Error fetching data:", e)
            return None

    def close_connection(self):
        if self.conn:
            try:
                self.conn.close()
                print("Connection to MariaDB database closed.")
            except mysql.connector.Error as e:
                print("Error closing connection to MariaDB database:", e)
        else:
            print("Connection to MariaDB database is not established.")
