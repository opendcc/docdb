import re
from typing import Union
from urllib.parse import urlparse, urlunparse

from playwright.sync_api import Page as SyncPage
from playwright.async_api import Page as AsyncPage

PageTypes = Union[SyncPage, AsyncPage]

class PageBase:
    def __init__(self, page: PageTypes, baseUrl: str):
        self.page = page
        self.url_pattern = re.compile(r"^.*" + baseUrl + r"([?].*)?$")
        self.base_url = baseUrl

    def is_on_page(self):
        return self.url_pattern.search(self.page.url)

    def get_base_url_without_query_parameters( self ):
        parsed_url = urlparse( self.base_url )
        new_url = urlunparse( ( parsed_url.scheme, parsed_url.netloc, parsed_url.path, '', '', '' ) )
        return( new_url )

    def reset_base_url( self, baseUrl: str ):
        self.base_url = baseUrl
