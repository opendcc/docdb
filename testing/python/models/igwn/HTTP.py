import re
import requests
from requests.auth import HTTPBasicAuth

from models.igwn.User import User

class HTTPPost:
    def __init__(
            self,
            user : User,
            url: str
    ):
        self.user = user
        self.url = url

    def commit(self, success_expression: str, exception ):
        # silence warnings about unverifiable ssl cert
        import urllib3

        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

        basic_auth = HTTPBasicAuth( self.user.username, self.user.password )

        r = requests.post(
            self.url,
            auth=basic_auth,
            data=self.payload,
            verify=False
        )
        self.response = r
        """
        success on matching:
                 You were successful. Your Document ID is <b>LIGO-{self.document_number}</b>.
        error message otherwise matches:
                 <dd>You seem to be attempting to create a duplicate of
                 document <a href="ShowDocument?docid=30"> M010001002</a>.
                 Please make sure that you have the correct document
                 number.</a></dd>
        """
        successful_regex = re.compile( success_expression )
        success_messages = re.findall(successful_regex, r.text)
        if len(success_messages) == 0:
            print(r.text)
            raise exception
        else:
            print(" ".join(success_messages))


if __name__ == "__main__":
    print("just litmus test that this module is not completely broken")
    pass
