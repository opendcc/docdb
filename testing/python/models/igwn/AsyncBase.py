import re
from models.igwn.PageBase import PageBase, AsyncPage

class AsyncBase(PageBase):
    def __init__(self, page: AsyncPage, baseUrl: str ):
        super().__init__(page, baseUrl)

    async def navigate(self):
        await self.page.goto(self.base_url)

    async def post(self):
        await self.navigate()  # TODO: make it an actual HTTP POST request
