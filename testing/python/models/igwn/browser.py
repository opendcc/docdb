import os
import pytest
import re

from playwright.async_api import async_playwright, Browser

from models.igwn.request_wrapper import request_wrapper as wrequest

class AsyncBrowser:
    def __init__(self, playwright, browser_name, browser_context_args, request: wrequest):
        self.assert_failed = False;
        self.playwright = playwright
        self.browser_context_args = browser_context_args
        self.request = request
        self.test_name = self.request.get_test_name( )
        self.browser_name = browser_name
        self.browser = None
        self.context = None
        self.page = None
        self.tracing_filename = None
        self.video_filename = None
        self.browser_context_args["record_video_dir"] = \
            self.request.get_output_dir( )
        if ( self.browser_name == "" ):
            self.browser_name = self.request.get_browser_name( )

    async def launch_browser(self):
        self.browser = await self.playwright[self.browser_name].launch()
        self.context = await self.browser.new_context(**self.browser_context_args)
        self.page = await self.context.new_page()
        # Specify the tracing file path and name
        self.tracing_filename = self.relative_to_output_dir( "trace.json" )
        await self.browser.start_tracing(page=self.page, path=self.tracing_filename )

    async def close_browser( self ):
        if self.page:
            # -----------------------------------------------------------
            # Preserve the tracing file
            # -----------------------------------------------------------
            await self.browser.stop_tracing( )
            if ( not self.request.should_retain_tracing( ) ):
                self.delete_tracing_file( )

            # -----------------------------------------------------------
            # Preserve screenshot
            # -----------------------------------------------------------
            if ( self.request.should_save_screenshot( ) ):
                screenshot_filename = self.relative_to_output_dir( "screenshot.png" )
                screenshot = await self.page.screenshot( )
                with open( screenshot_filename,
                           "wb") as file_stream:
                    file_stream.write( screenshot )
            # -----------------------------------------------------------
            # Preserve video
            # -----------------------------------------------------------
            await self.page.close()
            if self.page.video:
                await self.page.video.save_as(f"{self.test_name}-{self.browser_name}-async")
            if ( not self.request.should_retain_video( ) ):
                # Delete the video file if it exists (optional)
                self.delete_video_file()
        if self.context:
            await self.context.close()
        if self.browser:
            await self.browser.close()

    def delete_tracing_file(self):
        if self.tracing_filename and os.path.exists( self.tracing_filename ):
            os.remove( self.tracing_filename )

    def delete_video_file(self):
        if self.video_filename and os.path.exists( self.video_filename ):
            os.remove( self.video_filename )

    def relative_to_output_dir( self, path ):
        return( os.path.join( self.request.get_output_dir( ),
                              path ) )

    async def __aexit__(self, exc_type, exc_value, traceback):
        if exc_type is not None:
            # -----------------------------------------------------------
            #  There was an assertion failure so need to remember this
            #   for video recordings, screen snapshots, etc.
            # -----------------------------------------------------------
            self.assert_failed = True
        self.request.assert_failed = self.assert_failed
        await self.close_browser( )
