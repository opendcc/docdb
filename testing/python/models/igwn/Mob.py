import traceback

import asyncio
from typing import Dict, List

from playwright.async_api import async_playwright, Page

from models.igwn.LoginBase import LoginBase
from models.igwn.UserContext import UserContext
from models.igwn.request_wrapper import request_wrapper

class Mob:
    """A collection of UserContexts

    This maintains a collection of UserContexts to alleviate the need of
    cascading async with clauses.

    This class takes care of:
    >>>Creation of the playwright instance needed for async interface.
    >>>Creation of UserContext for each requested user.
    >>>As part of the UserContext, the user is logged into the system.

    Attributes:
        user_names (List[str]): List of user names to create
        browser_context_args: copy of browser_context_args fixture
        request: copy of request fixture
        page: copy of page fixture
        base_url (str): copy of base_url fixture
        users (Dict[str, UserContext]):
            collection of UserContext entries
            indexed by user_name
        login_class (LoginBase):
            If not None, then it is the class used
            to authenticate the use

    Example:
        >>> async with Mob( UserNames = [ "admin", "user1" ],
        >>>     BrowserContextArgs = self.browser_context_args,
        >>>     Request = self.request,
        >>>     Page = self.page,
        >>>     BaseURL = self.base_url ) as mob:
        >>>         document_number = self.random_document.document_number
        >>>         admin_page = DocumentAddForm(mob.context('admin').browser.page)
    """
    def __init__(self, UserNames : List[ str ],
                 BrowserContextArgs,
                 Request,
                 Page: Page,
                 BaseURL: str,
                 login_class : LoginBase = None ):
        """
        Create new instance of a Mob
        """
        self.user_names = UserNames
        self.browser_context_args = BrowserContextArgs
        self.request_wrapper = request_wrapper( Request )
        self.page = Page
        self.base_url = BaseURL
        self.login_class = login_class
        self.users: Dict[ str, UserContext ] = {}

    """Retrieve the UserContext associated with the given user

    Args:
        UserName (str):
            Name associated with a UserContext

    Returns:
        UserContext: UserContext associated with UserName

    Example:
        >>> async with Mob( UserNames = [ "admin", "user1" ],
        >>>     BrowserContextArgs = self.browser_context_args,
        >>>     Request = self.request,
        >>>     Page = self.page,
        >>>     BaseURL = self.base_url ) as mob:
        >>>         document_number = self.random_document.document_number
        >>>         admin_page = DocumentAddForm(mob.context('admin').browser.page)

    """
    def context( self, UserName: str ) -> UserContext:
        return( self.users.get( UserName ) )

    """Async initializer

    This initializes the attributes when called using async with syntax.

    Note:
        This is not intended to be called by user
    """
    async def __aenter__( self ):
        # Create an object and add it to the collection
        self.playwright = await async_playwright().start()
        for user_name in self.user_names:
            self.users[ user_name ] = \
                UserContext( request = self.request_wrapper,
                             playwright = self.playwright,
                             browser_context_args = self.browser_context_args,
                             base_url = self.base_url,
                             username = user_name,
                             login_class = self.login_class )
            await self.users[ user_name ].async_context_setup( )
        return self

    """Async cleanup

    This cleans up resources allocated by __aenter__

    Note:
        This is not intended to be called by user
    """
    async def __aexit__( self, exc_type, exc_value, traceback ):
        # Cleanup logic goes here
        # For example, you can clear the collection when exiting the 'async with' block
        for user in self.users.values( ):
            await user.async_context_cleanup( )
        self.users.clear()
        if self.playwright:
            await self.playwright.stop()


