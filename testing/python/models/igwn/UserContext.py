import pytest
from playwright.async_api import async_playwright

from models.igwn.request_wrapper import request_wrapper
from models.igwn.browser import AsyncBrowser
from models.igwn.LoginBase import LoginBase
from models.igwn.Users import Users

class UserContext:
    def __init__(self,
                 request,
                 playwright,
                 browser_context_args,
                 base_url,
                 username = "admin",
                 login_class : LoginBase = None ):
        request_info = None
        if ( isinstance( request, request_wrapper ) ):
            request_info = request
        else:
            request_info = request_wrapper( request )
        self.username = username
        self.base_url = base_url
        self.browser = AsyncBrowser( playwright = playwright,
                                     browser_name = 'chromium',
                                     browser_context_args = browser_context_args,
                                     request = request_info )
        self.login_class = login_class

    async def async_context_setup(self):
        await self.__aenter__( )
        return self

    async def async_context_cleanup(self):
        exc_type = None
        exc_value = None
        traceback = None
        if hasattr( self, 'exc_info' ) and self.exc_info:
            exc_type, exc_value, traceback = self.exc_info
        await self.__aexit__( exc_type, exc_value, traceback )

    async def __aenter__(self):
        await self.browser.launch_browser( )

        # -----------------------------------------------------------
        # Perform user-specific login actions
        # -----------------------------------------------------------
        page = self.browser.page;
        if ( page and self.login_class ):
            login_page = self.login_class( page, self.base_url )
            await login_page.login( Users.get( self.username ) )
        # -----------------------------------------------------------
        return self

    async def __aexit__(self, exc_type, exc_value, traceback):
        if self.browser.browser:
            await self.browser.__aexit__( exc_type, exc_value, traceback )
