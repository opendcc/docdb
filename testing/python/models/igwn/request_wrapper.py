import os
import re

class request_wrapper:
    def __init__(self, request):
        if ( isinstance( request, request_wrapper ) ):
            self.request = request.request
        else:
            self.request = request
        self.browser_name = None
        self.assert_failed = False
        if ( False ): # Debug
            print( f"request is of type: {type(request)}" )
            print( f"request_wrapper.request is of type: {type(self.request)}" )
            for attr in [ 'node', 'config' ]:
                if ( hasattr( self.request, attr ) ):
                    print( f"DEBUG: request: {attr}: present" )
                else:
                    print( f"DEBUG: request: {attr}: missing" )
            for attr in dir( self.request ):
                print( f"DEBUG: request.{attr}")

    def get_output_dir(self):
        retval = None
        if self.request.config.getoption("output"):
            retval = os.path.join(self.request.config.getoption("output"),
                                  f"{self.get_test_name().replace('_','-')}-{self.get_browser_name()}")
        return retval

    def get_browser_name(self):
        if self.browser_name:
            return self.browser_name
        browser = self.request.config.getoption("--browser")
        if browser:
            if isinstance(browser, list):
                browser = browser[0]
            elif issintance(browser,str):
                pass
            else:
                browser = None
        if not browser:
            browser = "chromium"
        self.browser_name = browser
        return self.browser_name

    @staticmethod
    def get_full_node_name(node):
        names = [node.name.lower( )]
        while node.parent and node.parent.parent:
            node = node.parent
            names.insert(0, node.name.lower( ))
        retval = '-'.join(names)
        retval = re.sub(r'[./_]', '-', retval)
        retval = re.sub(r'\[([^\]]*)\]', r'-\1', retval)
        return retval

    def should_retain( self, value ):
        return (
            ( value == "on") or
            ( ( value == "retain-on-failure" ) and ( self.assert_failed ) )
        )

    def should_save( self, value ):
        return (
            ( value == "on" ) or
            ( ( value == "only-on-failure" ) and ( self.assert_failed ) )
        )

    def should_save_screenshot( self ):
        return( self.should_save( self.get_screenshot( ) ) )

    def should_retain_tracing( self ):
        return( self.should_retain( self.get_tracing( ) ) )

    def should_retain_video( self ):
        return( self.should_retain( self.get_video( ) ) )

    def get_test_name(self):
        retval = ""
        try:
            retval = self.get_full_node_name( self.request.node )
        except:
            reval = "UnknownTestName"
        return( retval )

    def get_screenshot(self):
        return( self.request.config.getoption("--screenshot") )

    def get_tracing(self):
        return( self.request.config.getoption("--tracing") )

    def get_video(self):
        return( self.request.config.getoption("--video") )

    def __str__(self, debugging_info : bool = False ):
        if ( debugging_info ):
            return( f"request_wrapper: request: {str(self.request)}" )
        else:
            return( f"{str(self.request)}" )
