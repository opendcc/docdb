from models.igwn.DatabaseHandler import DatabaseHandler

class SQLiteHandler(DatabaseHandler):
    def __init__(self, db_file):
        self.db_file = db_file
        self.conn = None

    def connect(self):
        # SQLite connection logic
        pass

    def execute_query(self, query, params=None):
        # SQLite query execution logic
        pass

    def fetch_data(self, query, params=None):
        # SQLite data fetching logic
        pass

    def close_connection(self):
        # SQLite connection closing logic
        pass
