from abc import ABC, abstractmethod

class DatabaseHandler(ABC):
    @abstractmethod
    def connect(self):
        pass

    @abstractmethod
    def execute_query(self, query, params=None):
        pass

    @abstractmethod
    def fetch_data(self, query, params=None):
        pass

    @abstractmethod
    def close_connection(self):
        pass

