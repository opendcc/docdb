import ldif
from os import environ

from models.igwn.default_secrets import *

try:
    from secrets import *
except:
    pass

def is_batch():
    return environ.get("IGWN_WEBAPP_TESTING_MODE", "interactive") == "batch"

def read_ldif_element( entry, name: str ):
    retval = None
    if ( name in entry ):
        retval = entry[name][0]
        try:
            retval = retval.decode('utf-8')
        except:
            pass
    return( retval )

def read_ldif( stream ):
    entries = {}
    try:
        parser = ldif.LDIFRecordList(stream)
        parser.parse( )
        entries = parser.all_records
    except Exception as error:
        parser = ldif.LDIFParser(stream)
        entries = parser.parse( )
    return entries

if __name__ == "__main__":
    pass
