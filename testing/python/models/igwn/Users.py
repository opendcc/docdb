import re
from faker import Faker
from os import environ
from typing import Type

from tests.project import DEFAULT_BATCH_USER
from tests.project import DCC_CONFIGURATION_DEFAULT

users = {}

from models.igwn.User import User
from models.igwn.Utilities import is_batch, read_ldif, read_ldif_element

class Users( ):
    DEFAULT_BATCH_USER = "admin"
    user_collection = {}

    def __init__( self ):
        self.load_users( )

    @classmethod
    def create_user( cls: Type['Users'],
                     cn: str = None,
                     first_name: str = None,
                     last_name: str = None,
                     username: str = None,
                     email:str = None,
                     password: str = None,
                     context: str = None ):
        """
        """
        #----------------------------------------------------------------
        # Pattern used to select user from drop down list
        #   On(e)
        #   Tw(o)
        #   Thr(ee)
        #----------------------------------------------------------------
        pattern = re.compile( r'on|tw|thr', re.IGNORECASE )
        if ( not first_name ):
            #----------------------------------------------------------
            # Need to ensure first name does not match common patterns
            #  used to select user from list
            #----------------------------------------------------------
            while True:
                first_name = Faker( ).first_name( )
                if not re.search( pattern, first_name ):
                    break
        if ( not last_name ):
            #----------------------------------------------------------
            # Need to ensure last name does not match common patterns
            #  used to select user from list
            #----------------------------------------------------------
            while True:
                last_name = Faker( ).last_name( )
                if not re.search( pattern, last_name ):
                    break
        cn = cn or f"{first_name} {last_name}"
        username = username or f"{first_name.lower( )}.{last_name.lower( )}"
        email = email or f"{username}@ligo.org"
        retval = cls.user_collection[cn] = \
            User( cn=cn,
                  username=username,
                  password=password,
                  email=email,
                  context=context )
        retval.first_name = first_name
        retval.last_name = last_name
        return( retval )

    @classmethod
    def empty(cls: Type['Users']):
        return( bool(cls.user_collection) == False)

    @classmethod
    def load_users(cls: Type['Users']):
        """
        Mehtod to load all users into the class

        Args:
            cls (Type['Users']): The class
        """
        if (not cls.empty()):
            return
        DCC_CONFIGURATION = environ.get(
            "DCC_CONFIGURATION", DCC_CONFIGURATION_DEFAULT
        )
        filename = f"{DCC_CONFIGURATION}/users.ldif"
        with open(filename, "rb") as stream:
            entries = read_ldif(stream)
            try:
                for dn, entry in entries:
                    cn = read_ldif_element( entry, "cn" )
                    email = read_ldif_element( entry, "mail" )
                    username = read_ldif_element( entry, "uid" )
                    password = read_ldif_element( entry, "userPassword" )
                    context = read_ldif_element( entry, "context" )
                    if username is not None:
                        cls.user_collection[cn] = \
                            User( cn=cn,
                                  username=username,
                                  password=password,
                                  email=email,
                                  context=context )
            except Exception as error:
                print(f"Error Parsing {filename}: {error}")

    @classmethod
    def get(cls: Type['Users'], username: str = None ) -> User:
        cls.load_users( )
        if ( username is None ):
            if is_batch( ):
                #------------------------------------------------------------
                # Running in batch mode.
                #------------------------------------------------------------
                username = DEFAULT_BATCH_USER
            else:
                #------------------------------------------------------------
                # For non-batch, take the first user from the legacy users
                #------------------------------------------------------------
                global users

                if ( users ):
                    username = list(users.keys( ))[0]
                    if ( not username in cls.user_collection ):
                        cls.user_collection[ username ] = \
                            User( cn=users[ username ].cn,
                                  username=users[ username ].username,
                                  password=users[ username ].password,
                                  email=users[ username ].email,
                                  context=context[ username ].context )
        if ( username in cls.user_collection):
            return cls.user_collection[ username ]
        return( None )

if __name__ == "__main__":
    pass
