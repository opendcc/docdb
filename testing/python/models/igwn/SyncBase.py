import re
from models.igwn.PageBase import PageBase, SyncPage

class SyncBase(PageBase):
    def __init__(self, page: SyncPage, baseUrl: str ):
        super().__init__(page, baseUrl)

    def navigate(self):
        self.page.goto(self.base_url)

    def post(self):
        self.navigate()  # TODO: make it an actual HTTP POST request
