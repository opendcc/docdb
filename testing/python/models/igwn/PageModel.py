import asyncio

from models.igwn.PageBase import SyncPage, AsyncPage, PageTypes
from models.igwn.SyncBase import SyncBase
from models.igwn.AsyncBase import AsyncBase

class PageModel:
    def __init__(self, page: PageTypes, baseUrl: str):
        self.page = page
        self.model_is_async = False
        if isinstance(page, SyncPage):
            self.base = SyncBase(page, baseUrl)
        elif isinstance(page, AsyncPage):
            self.model_is_async = True
            self.base = AsyncBase(page, baseUrl)
        else:
            raise ValueError("Unsupported type")

    def __getattr__(self, attr):
        return getattr(self.base, attr)

    def is_on_page(self):
        return self.base.is_on_page( )

class PageModelSyncBase( PageModel, SyncBase):
    def __init(self, page: PageTypes, baseUrl:str):
        SyncBase.__init__(page, baseUrl)

class PageModelAsyncBase( PageModel, AsyncBase):
    def __init(self, page: PageTypes, baseUrl:str):
        AsyncBase.__init__(page, baseUrl)
