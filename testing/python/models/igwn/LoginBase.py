from abc import ABC, abstractmethod
from typing import Union

from models.igwn.PageBase import PageTypes, SyncPage, AsyncPage
from models.igwn.PageModel import PageModel
from models.igwn.User import User

class LoginBase( ABC, PageModel ):
    def __init__(self, page: PageTypes, baseUrl: str ):
        super().__init__(page, baseUrl)
        #----------------------------------------------------------------
        # Establish aliases to sync and async methods to provide
        #   a single look and feel
        #----------------------------------------------------------------
        if isinstance(page, SyncPage):
            self.login = self._sync_login
        elif isinstance(page, AsyncPage):
            self.login = self._async_login
        else:
            raise ValueError("Unsupported type")

    @abstractmethod
    def _sync_login(self, user: Union[str, User]):
        pass

    @abstractmethod
    async def _async_login(self, user: Union[str, User]):
        pass
