class User( ):
    """
    Keep track of user characteristics

    This class maintains a collection of attributes associated with users

    Attributes:
        username (str): User account name
        password (str): Password for user account
        email (str): email associate with user account
        context (str): FIXME
        cn (str): FIXME
    """
    def __init__(self, username: str, password: str, email: str, context: str, cn: str ):
        self.username = username
        self.password = password
        self.email = email
        self.context = context
        self.cn = cn

    def __str__(self):
        return( f"User instance with values:\n"
                f"\tcn={self.cn}\n"
                f"\tusername={self.username}\n"
                f"\tpassword={self.password}\n"
                f"\temail={self.email}\n"
                f"\tcontext={self.context}"
               )

if __name__ == "__main__":
    pass
