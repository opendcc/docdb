from models.PageWithTitleModel import PageWithTitleModel
import re
import pytest


class ShowDocument(PageWithTitleModel):
    """
    aka DocumentCard, usually accessed via a smart url such as https://dcc/LIGO-M210003-x0

    Attributes:
        signoff_info (locator): Collection of signoff information.
            The last one in the list is the final approver.
            The information appears surrounded by parentheses and takes onen of the following forms:
            - (Signed on YYYY-MM-DD)
            - ()
    """

    signature_cache = dict()

    def __init__( self, page, dialog_action = 'accept' ):
        super().__init__(page, "/cgi-bin/private/DocDB/ShowDocument", "LIGO Document")
        #----------------------------------------------------------------
        # fixup sync/async methods to have a uniform api as much as possible
        #----------------------------------------------------------------
        if self.model_is_async:
            self.navigate = self._async_navigate
        else:
            self.navigate = self._sync_navigate
        #----------------------------------------------------------------
        self.dialog_action = dialog_action
        self.abstract = page.locator("div#Abstract")
        self.authors = page.locator("div#Authors")
        self.viewable = page.locator("#Viewable")
        self.modifiable = page.locator("#Modifiable")
        self.access_denied_message = page.get_by_text(
            "is not authorized to view this document"
        )
        #----------------------------------------------------------------
        # TODO: more widgets whose presence to assert
        # self.signoff_block = page.get_by_text('Signoffs:')
        #----------------------------------------------------------------
        self.signature_text_area = page.locator("#signofflist")
        self.signoff_block = page.locator('div[data-cy="OpenForSig"]')
        self.waiting_signature_message = page.get_by_text("waiting for signature")
        self.waiting_other_signature_message = page.get_by_text(
            "waiting for other signature"
        )
        self.waiting_approval_message = page.get_by_text("waiting for approval")
        self.signature_warning_message = page.get_by_text(
            "The signature process for this document is closed"
        )
        #----------------------------------------------------------------
        # Buttons
        #----------------------------------------------------------------
        self.clone_button = page.get_by_role( 'button', name='Clone Document' )
        self.goto_new_document_button = page.get_by_role( 'button', name = 'Go To New Document' )
        self.sign_button = page.locator('[data-cy="SignButton"]')
        self.unsign_button = page.locator('[data-cy="UnSignButton"]')
        self.change_metadata_button = page.locator('input[type=submit][value="Change Metadata"]')
        self.deny_button = page.locator('[data-cy="DenyButton"]')
        self.abstain_button = page.locator('[data-cy="AbstainButton"]')
        self.approve_button = page.locator('input[data-cy="ApproveButton"]')
        self.disapprove_button = page.locator('input[data-cy="DisapproveButton"]')
        self.unapprove_button = page.locator('input[data-cy="ApproverUnsignButton"]')
        #----------------------------------------------------------------
        # Checkboxes
        #----------------------------------------------------------------
        self.parallel_signoff_checkbox = page.locator("input[name=parallelsignoff]")
        #----------------------------------------------------------------
        # List Items
        #----------------------------------------------------------------
        self.signoff_info = page.locator('[data-cy="SignoffInfo"]')
        # self.signoff_info = page.get_by_role( "listitem", name="SignoffInfo" )

        #================================================================
        # Handling of the popup dialog that happens for:
        #   Clone Document
        #----------------------------------------------------------------
        def handle_dialog_sync( dialog ):
            if ( self.dialog_action == 'accept' ):
                dialog.accept( )
            elif ( self.dialog_action == 'cancel' ):
                dialog.dismiss( )
        # Handling of the popup dialog that happens for:
        #   Clone Document
        async def handle_dialog_async( dialog ):
            if ( self.dialog_action == 'accept' ):
                await dialog.accept( )
            elif ( self.dialog_action == 'cancel' ):
                await dialog.dismiss( )
        if self.model_is_async:
            self.page.on( "dialog", handle_dialog_async )
        else:
            self.page.on( "dialog", handle_dialog_sync )

    def not_authorized_to_view(self, username: str = None):
        return self.page.get_by_text(
            f"User {username} is not authorized to view this document."
        )

    def _sync_navigate(self, doc_alias: str):
        self.page.goto(f"/{doc_alias}")

    async def _async_navigate(self, doc_alias: str):
        await self.page.goto(f"/{doc_alias}")

    # TODO: DELETE sign_on_bahalf_of and cache_signature_details_for once tests for #500 and #566 are converted
    # to multiple async browser conexts
    def cache_signature_details_for(self, user: str = ""):
        self.signature_cache[user] = {
            "doc_id": self.page.locator("[name=docid]").first.input_value(),
            "version": self.page.locator("[name=version]").first.input_value(),
            "signatureid": self.page.locator("[name=signatureid]").input_value(),
            "emailuserid": self.page.locator("[name=emailuserid]").input_value(),
        }

    def sign_on_behalf_of(
        self,
        user: str = "",
        action: str = "approve",
        users: dict = None,
        base_url: str = "https://www.dcc.ligo.org",
    ):
        assert user in users
        form_data = self.signature_cache[user]
        form_data["sign_action"] = action
        from requests import post
        from requests.auth import HTTPBasicAuth

        basic = HTTPBasicAuth(users[user]["username"], users[user]["password"])
        import urllib3

        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

        resp = post(
            f"{base_url}/cgi-bin/private/DocDB/SignRevision",
            auth=basic,
            data=form_data,
            verify=False,
        )
        assert resp.ok
