from models.PageWithTitleModel import PageWithTitleModel
import re
import pytest

class ShowCalendar( PageWithTitleModel ):
    """
    """
    def __init__(self, page ):
        super().__init__( page,
                          "/cgi-bin/private/DocDB/ShowCalendar",
                          "Calendar for")
        self.today = page.locator( 'td.Today > a.Date' )

    def DateURL( self,
                 year: int = None,
                 month: int = None,
                 day: int = None
                 ):
        if ( year and month and day ):
            base_url_without_query_parameters = self.get_base_url_without_query_parameters( )
            self.reset_base_url( f"{base_url_without_query_parameters}?month={month}&day={day}&year={year}" )
