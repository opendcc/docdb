from models.PageWithTitleModel import PageWithTitleModel
import pytest


class ListAllMeetings( PageWithTitleModel ):
    """
    """

    def __init__(self, page):
        super().__init__( page, "/cgi-bin/private/DocDB/ListAllMeetings", "List of Events" )
        self.button_business_office = \
            page.get_by_role( "link", name="Business Office", exact=True )
        self.button_collaboration_meetings = \
            page.get_by_role( "link", name="Collaboration Meetings", exact=True )
        self.button_conferences = \
            page.get_by_role( "link", name="Conferences", exact=True )
        self.button_lvk_operations = \
            page.get_by_role( "link", name="LVK Operations", exact=True )
        self.button_procurement_non_nsf = \
            page.get_by_role( "link", name="Procurement - Non-NSF", exact=True )
        self.button_procurement_operations = \
            page.get_by_role( "link", name="Procurement - Operations", exact=True )
        self.button_procurement_operations_india = \
            page.get_by_role( "link", name="Procurement - Operations - India", exact=True )
        self.button_procurement_operations_fy2015plus = \
            page.get_by_role( "link", name="Procurement - Operations FY2015+", exact=True )
        self.button_procurement_other_nsf_grants = \
            page.get_by_role( "link", name="Procurement - Other NSF Grants", exact=True )
        self.button_procurement_aligo = \
            page.get_by_role( "link", name="Procurement - aLIGO", exact=True )
        self.button_reviews = \
            page.get_by_role( "link", name="Reviews", exact=True )
        self.button_seminars = \
            page.get_by_role( "link", name="Semniars", exact=True )
        self.button_aligo_activities = \
            page.get_by_role( "link", name="aLIGO Activities", exact=True )
        self.button_other = \
            page.get_by_role( "link", name="other", exact=True )

    def button_ligo_number( self, ligo_number ):
        return self.page.get_by_role( "link", name=f'{ligo_number}' )
