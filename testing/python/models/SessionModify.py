from models.PageWithTitleModel import PageWithTitleModel

class SessionModify( PageWithTitleModel ):
    """
    """

    def __init__(self, page):
        super().__init__( page, "/cgi-bin/private/DocDB/SessionModify", "Create a New Event" )
        self.create_new_event = page.locator('//a[normalize-space(text())="Add a New Event on this Day"]')
        self.event_groups = page.locator('select[name=eventgroups]')
        self.topics = page.locator( 'select[name=topics]' )
        self.button_create_event_and_talks = page.get_by_role( "button", name="Create Event and Talks").first
        self.button_modify_event_and_talks = page.get_by_role( "button", name="Modify Event and Talks").first
        self.added_item_to_agenda_first_talk = page.locator( 'dd:has-text("Added item to agenda: First Talk")' )
        self.created_new_event_autocomplete = page.locator( 'dd:has-text("Created new event: Autocomplete")' )
        self.description_long = page.locator( 'input[name=long]' )
        self.description_short = page.locator( 'input[name=shortdesc]' )
        self.display_the_event = page.locator( 'a:has-text("Display the Event")' )
        self.instructions = page.locator('a:has-text("instructions")')
        self.list_authors_n1 = page.locator('textarea#list_authors-n1' )
        self.list_of_moderators = page.locator( 'textarea#list_moderators' )
        self.location = page.locator( 'input[name=location]' )
        self.session_description = page.locator( 'textarea[name=sessiondescription]' )
        self.talk_title = page.locator( 'input[name=talktitle]' )
        self.url = page.locator( 'input[name=url]' )
        self.user_suggestions = page.locator( 'li[role=menuitem]' )

    def NewSessionURL( self,
                   singlesession : int = None,
                   day : int = None,
                   month : int = None,
                   year : int = None ):
        if ( singlesession and day and month and year ):
            base_url_without_query_parameters = self.get_base_url_without_query_parameters( )
            self.reset_base_url( f"{base_url_without_query_parameters}?mode=new&singlesession={singlesession}&sessionday={day}&sessionmonth={month}&sessionyear={year}" )

    def SelectEventGroup( self, group: str ):
        self.page.select_option( 'select[name=eventgroups]', group )

    def created_new_event( self, event: str ):
        return( self.page.locator( f'dd:has-text("Created new event: {event}")' ) )
