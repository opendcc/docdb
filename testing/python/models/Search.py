from models.PageWithTitleModel import PageWithTitleModel


class Search(PageWithTitleModel):
    def __init__(self, page):
        super().__init__(
            page, "/cgi-bin/private/DocDB/SearchForm", "Document Database Search"
        )
