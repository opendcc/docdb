from models.PageWithTitleModel import PageWithTitleModel


class ReserveNumber(PageWithTitleModel):
    def __init__(self, page):
        super().__init__(
            page, "/cgi-bin/private/DocDB/ReserveHome", "Reserve a Document Number"
        )
        self.basic_search_button = page.locator('input[value="Basic Search"]')
        self.number_search_button = page.locator('input[value="Number Search"]')
        self.use_button = page.locator('input[value="Use"]')
        self.reserve_button = page.locator('input[value="Reserve"]')
