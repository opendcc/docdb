from faker import Faker
from typing import Union

"""
some inits : Faker
"""
Faker.seed()


class Document:
    """
    This captures details related to documents.

    When testing the system, it is often necessary to create a document
    as part of the test.
    This class aids in the management of the details related to the
    document allowing some parts to be generated randomly and
    other parts having manual overrides.

    Attributes:
        title (str): Document title
        abstract(str): Document abstract
        requester (int): Requester
        keywords (str): Space separated string of keywords
        doc_type (str): Ascii character for the type of document
        security (int): FIXME
        modify (int): FIXME
        authors (List[str]): List of authors specified by "last_name, first_name"
        topics (int): FIXME
        document_number (int): Numerical number associated with the document

    Related Classes:
        - models.DocumentAddForm: Use the create_document method to create
          a document with the characteristics of the object
    """
    class bimap:
        def __init__( self, seed: dict = {} ):
            self.data = {}
            self._inverse = {}
            for key, value in seed.items( ):
                self.data[ key ] = value
                self._inverse[ value ] = key

        @property
        def inverse(self):
            return self._inverse.copy( )

        def __getitem__(self, key):
            return self.data[key]

        def __setitem__(self, key, value):
            self.data[key] = value
            self._inverse[ value ] = key

        def __delitem__(self, key):
            del self._inverse[ self.data[key] ]
            del self.data[key]

        def __iter__(self):
            return iter(self.data)

        def __len__(self):
            return len(self.data)

    DOC_TYPES = bimap( {
        "!": 0,
        "C": 1,
        "D": 2,
        "?": 3,
        "E": 4,
        "G": 5,
        "L": 6,
        "M": 7,
        "P": 8,
        "S": 9,
        "T": 10,
        "F": 11,
        "Q": 12,
        "X": 13,
        "R": 14,
        "A": 15,
        "I": 16,
    } )

    MAX_WIDTH = 8

    def __init__(
            self,
            random: bool = True,
            requester: int = 3,
            document_number: int = -1,
            prefix: int = -1,
            doc_type = "T"
    ):
        """
        Initialize the instance

        Parameters:
            random (bool): Should random values be generated for the document
            requester (int): Individual making the request
            document_number (int): Number to assign to the document
            prefix (int): Number to appear on the far left hand side of the document_number

        See Also:
            reset_doc_number:
        """
        if ( random ):
            self.title = Faker( ).sentence( )
            self.abstract = Faker( ).paragraph( )
        else:
            self.title = "Second doc"
            self.abstract = "This doc was POSTed directly from playwright, using requests"
        self.requester = requester
        self.keywords = "playwright POST automation test document "
        self._doc_type = doc_type
        self.security = 2
        self.modify = 2
        self.authors = ["One, User", "Istrator, Admin"]
        self.topics = 59
        self._doc_numeric = -1
        self.reset_doc_number( number = document_number, prefix=prefix )

    def authors_as_string( self ):
        """
        Format the list of authors for web forms

        This function returns the list of authors as required by web forms
        used by the DCC web application.

        Returns:
            str: formatted list of authors
        """
        # TODO: sanity checking, will do as is for now
        return( "\n".join( self.authors ) )

    def doc_number_prefix( self, prefix: int ):
        """
        Reset the high order digits of the document number

        Parameters:
            prefix (int): New most significant digits for the document number
        """
        self.reset_doc_number( number = self._doc_numeric, prefix=prefix )

    @property
    def doc_type( self ):
        return( self._doc_type )

    @doc_type.setter
    def doc_type( self, value ):
        if( ( len(value) == 1) and
            ( value in self.__class__.DOC_TYPES.data.keys( ) ) ):
            self._doc_type = value
            # Update doc_number
            self.doc_number = value + self.doc_number[1:]

    def doc_type_as_number( self ):
        if ( type( self.doc_type) is str ):
            return(  self.__class__.DOC_TYPES[ self.doc_type ] )
        else:
            return( doc_type )

    @property
    def document_number( self ):
        """
        Return the doc_number

        This provides bacwards compatibility with the previous implementation
        of the Document class
        """
        return( self.doc_number )

    def reset_doc_number( self, number: int = -1, prefix: int = -1 ):
        if ( number >= 0 ):
            # Trim the number
            self._doc_numeric = number**(self.__class__.MAX_WIDTH - 1)
        else:
            fake = Faker()
            self._doc_numeric = int( f"{fake.msisdn()}"[:self.__class__.MAX_WIDTH - 1] )
        self.doc_number = (f"{self.doc_type}" + \
            f"{self._doc_numeric:0{self.__class__.MAX_WIDTH - 1}d}")\
            [:self.__class__.MAX_WIDTH]
        if ( prefix >= 0 ):
            # Determine the length of the prefix
            prefix_string = f"{prefix}"
            replacement_length = len( prefix_string )

            # Squeeze in the prefix
            self.doc_number = \
                self.doc_number[:1] \
                + prefix_string \
                + self.doc_number[1 + replacement_length:]
        self._doc_numeric = int(self.doc_number[1:])
        return( self.doc_number )

    def full_name( self ):
        document_name = f"LIGO-{self.doc_number}"
        return( document_name )

    def DocumentType(self, letter: str = None):
        if ( letter in Document.DOC_TYPES.data ):
            # python ain't no javascript man!
            self.doc_type = Document.DOC_TYPES[letter]
            self.doc_number= f"{letter}{self.doc_number[1:]}"
        return self.doc_type


if __name__ == "__main__":
    print("just litmus test that this module is not completely broken")
    doc1 = Document( document_number=45, prefix=9 )
    doc2 = Document( random=True )
    doc2.DocumentType("T")
    doc2.Authors(
        [
            "Istrator, Admin",
            "Two, User",
        ]
    )
    # print(doc2.fields)
    doc3 = Document( document_number=43, prefix=93 )

    print( f"doc1: doc_number: {doc1.doc_number}" )
    print( f"doc2: doc_number: {doc2.doc_number}" )
    print( f"doc3: doc_number: {doc3.doc_number}" )
    print( f"doctype: T(10): {Document.DOC_TYPES['T']}" )
    print( f"doc3: doctype: {doc3.doctype_as_number( )}" )
