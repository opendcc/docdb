import re
import requests
from os import environ
from typing import Pattern

"""
this module is probably poorly named: it aims at providing means to query mailhog / mailpit
in order to test email traffic coming from DCC
And to do so by leveraging the smtp pseudo server's API
"""


class MailMessage:
    messages = []

    # FIXME:  skip the creation of the MailHog object and have a class method that does the proper API call

    @classmethod
    def fromMailServer(cls, apiurl: str = environ["MAILHOG_URL"]):
        # TODO: ? say 'HELO' to the stmp port (not currently part of the signature of the constructor) and
        #  leverage that mailpit has a /api/vi/info endpoint, while mailhog never will

        r = requests.get(f"{apiurl}/api/v1/info")
        if r.ok:
            # TODO: assume MailPit
            raise NotImplementedError
        else:
            # assume MailHog
            return MailMessage().fromMailHog(
                MailHog(environ["MAILHOG_URL"]).messages(limit=200)
            )

    def fromMailHog(self, mh_response: dict):
        def mapper(mail_item: dict):
            assert "Content" in mail_item
            content = mail_item["Content"]
            assert "Body" in content
            assert "Raw" in mail_item
            raw = mail_item["Raw"]
            assert "Data" in raw
            assert "To" in raw
            assert "From" in raw
            assert "Created" in mail_item
            assert "Subject" in content["Headers"]
            # yes! I AM cherry picking from the RAW aspect of the mail item or the processed one (CONTENT)
            # and yes, a list would probably make as much sense as a generator
            for recipient in raw["To"]:
                yield {
                    "To": recipient,
                    "From": raw["From"],
                    "Created": mail_item["Created"],
                    "Subject": "\n".join(content["Headers"]["Subject"]),
                    "Body": content["Body"],
                }

        self.messages = [
            msg for mail_item in mh_response["items"] for msg in mapper(mail_item)
        ]
        return self

    def select(self, kind: str = None, pattern: Pattern[str] = None):
        """
        filters messages using regex

        :param kind: enumm [None, Any, date, sender, recipient, subject, body]
            None equivalent to Any: match the pattern on all available fields
            other match the patternn only to the named field
        :param pattern: regex to match
        :return: MailMessage object with the list of messages matching the pattern
        """
        kind_2_dict = {
            "date": "Created",
            "sender": "From",
            "recipient": "To",
            "subject": "Subject",
            "body": "Body",
        }
        resp = MailMessage()
        if kind is None:
            # FIXME: does not seem to work
            multiline_pattern = re.compile(pattern, re.MULTILINE)
            resp.messages = list(
                filter(
                    lambda m: any(
                        [
                            re.match(multiline_pattern, m[field])
                            for field in kind_2_dict.values()
                        ]
                    ),
                    self.messages,
                )
            )
        else:
            # TODO: raise a better exception if condition not met
            assert kind in kind_2_dict
            field = kind_2_dict[kind]
            resp.messages = list(
                filter(lambda m: re.match(pattern, m[field]), self.messages)
            )
        return resp

    def contains(self, kind: str = None, term: str = None):
        """
        filter messages using a simple string. could be implemented with a call of the `select' method
        :param kind: enumm [ date, sender, recipient, subject, body]
            None equivalent to Any: match the pattern on all available fields
            other match the patternn only to the named field
        :param pattern: regex to match
        :return: MailMessage object with the list of messages matching the pattern
        """
        assert term is not None
        kind_2_dict = {
            "date": "Created",
            "sender": "From",
            "recipient": "To",
            "subject": "Subject",
            "body": "Body",
        }
        resp = MailMessage()
        if kind is None:
            resp.messages = list(filter(lambda m: term in str(m), self.messages))
        else:
            # TODO: raise a better exception if condition not met
            assert kind in kind_2_dict
            field = kind_2_dict[kind]
            resp.messages = list(filter(lambda m: term in m[field], self.messages))
        return resp

    def count(self) -> int:
        return len(self.messages)


class MailHog:
    """
    dead simple implementation of the mailhogv2 api as defined in swagger
    (relative) smarts are in the above class
    """

    def __init__(self, apiurl: str = None):
        self.apiurl = environ["MAILHOG_URL"] if apiurl is None else apiurl

    # directly from the swagger https://raw.githubusercontent.com/mailhog/MailHog/master/docs/APIv2/swagger-2.0.yaml
    def search(
        self,
        kind: str = "containing",
        query: str = None,
        start: int = 0,
        limit: int = 50,
    ) -> dict:
        """
        uses mailhog api v2 to search for messages
        :param kind: str 'to' 'from' 'containing' defines the scope of the search
        :param query: str term to match
        :param start: int defaults to 0
        :param limit: int defaults to 50
        :return: dictionary that matches the response in swagger
        """
        r = requests.get(
            f"{self.apiurl}/api/v2/search?kind={kind}&query={query}&start={start}&limit={limit}"
        )
        return r.json()

    # directly from the swagger https://raw.githubusercontent.com/mailhog/MailHog/master/docs/APIv2/swagger-2.0.yaml
    def messages(self, start: int = 0, limit: int = 50) -> dict:
        """
        uses mailhog api v2 to retrieve messages
        :param start: int defaults to 0
        :param limit: int defaults to 50
        :return: dictionary that matches the response in swagger
        """
        r = requests.get(f"{self.apiurl}/api/v2/messages?start={start}&limit={limit}")
        return r.json()


if __name__ == "__main__":
    print("obligatory litmus test")
    mh = MailHog()
    from pprint import pprint

    msg1 = mh.messages(start=0, limit=2)
    # pprint(msg1)
    assert msg1["count"] == min(msg1["total"], 2)
    assert len(msg1["items"]) == msg1["count"]
    msg2 = mh.search(kind="to", query="user1", start=0, limit=2)
    assert msg2["count"] < 3
    assert len(msg2["items"]) < 3
    pprint(msg2["items"][0])
    print("=" * 50)
    mlm = MailMessage().fromMailHog(msg2)
    pprint(mlm.contains(kind="recipient", term="admin").messages)
    pprint(mlm.contains(term="dcc").count())
    print("=" * 50)
    print("=" * 50)
    print("=" * 50)
    pprint(mlm.select(kind="recipient", pattern=r"(user1|user2)").count())
    pprint(mlm.select(pattern=r"admin").count())
    print("-*- " * 15)
    mlm2 = MailMessage.fromMailServer(apiurl=environ["MAILHOG_URL"]).contains(
        kind="recipient", term="admin"
    )
    pprint(mlm2.messages)
