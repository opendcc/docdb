from faker import Faker
from models.igwn.MariaDB import MariaDB

"""
CREATE TABLE `Author` (
  `AuthorID` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(100) NOT NULL,
  `MiddleInitials` varchar(16) DEFAULT NULL,
  `LastName` varchar(100) NOT NULL,
  `InstitutionID` int(11) NOT NULL DEFAULT 0,
  `Active` int(11) DEFAULT 1,
  `TimeStamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `AuthorAbbr` varchar(10) DEFAULT NULL COMMENT 'Used in the Old DCC',
  `FullAuthorName` varchar(256) DEFAULT NULL COMMENT 'Used in the Old DCC',
  PRIMARY KEY (`AuthorID`),
  KEY `Name` (`LastName`)
) """


class Author:
    FirstName: str = ""
    LastName: str = ""

    def __init__(self, first_name: str = None, last_name: str = None):
        fake = Faker()
        self.FirstName = fake.first_name() if first_name is None else first_name
        self.LastName = fake.last_name() if last_name is None else last_name
        # FIXME: need safeguard if both first and last name are ''

    def __repr__(self) -> str:
        return f"{self.LastName}, {self.FirstName}"

    def commit(self, conn=None):
        with conn.cursor() as cur:
            sql = """
                INSERT INTO Author (FirstName,LastName, FullAuthorName, Active, InstitutionID )
                VALUES (?, ?, ?, 1, 26);
                """
            cur.execute(sql, [self.FirstName, self.LastName, f"{self}"])
            cur.execute(
                "select MAX(AuthorID) from Author where FirstName = ? and LastName =?",
                (self.FirstName, self.LastName),
            )
            self.AuthorID = cur.fetchone()[0]
        conn.commit()

    def create_in_dcc(self, conn=None):
        self.commit(conn)


class User(Author):
    """
    create Author,EmailUser and RemoteUser
    needs first, last, employeeID. capabilities, groups
    """

    FirstName: str = ""
    LastName: str = ""
    employeeNumber: int = 0

    def __init__(self, first_name: str = None, last_name: str = None):
        fake = Faker()
        self.FirstName = fake.first_name() if first_name is None else first_name
        self.LastName = fake.last_name() if last_name is None else last_name
        self.employeeNumber = fake.pyint(min_value=4001, max_value=9999)
        # FIXME: need safeguard if both first and last name are ''

    def __repr__(self) -> str:
        return f"{self.LastName}, {self.FirstName}"

    def groups(self, groupnames: list[str]):
        """
        sets the EmailUser usergroups the list passed in parameter. extra groups are removed
        """
        pass

    def commit(self, conn=None):
        with conn.cursor() as cur:
            sql_author = """
                INSERT INTO Author (FirstName,LastName, FullAuthorName, Active, InstitutionID )
                VALUES (?, ?, ?, 1, 26);
                """
            cur.execute(sql_author, [self.FirstName, self.LastName, f"{self}"])
            cur.execute(
                "select MAX(AuthorID) from Author where FirstName = ? and LastName =?",
                (self.FirstName, self.LastName),
            )
            self.AuthorID = cur.fetchone()[0]
            sql_email_user = """
                    INSERT INTO EmailUser (Username, Name, EmailAddress, AuthorID, CanSign, EmployeeNumber)
                    SELECT lower(concat(FirstName,'.',LastName)),
                        concat(FirstName,' ', LastName),
                        lower(concat(FirstName,'.',LastName, '@local')),
                        AuthorID, 1, ?
                    FROM Author
                    WHERE AuthorID = ? ;
            """
            cur.execute(sql_email_user, [self.employeeNumber, self.AuthorID])
            sql_remote_user = """
                    INSERT INTO RemoteUser ( RemoteUserName, EmailAddress, EmailUserID)
                        SELECT Username, EmailAddress, EmailUserID
                        FROM EmailUser
                        WHERE AuthorID = ? ;
            """
            cur.execute(sql_remote_user, [self.AuthorID])
        conn.commit()


if __name__ == "__main__":
    names_list = (
        (None, None),
        (None, None),
        (None, None),
        (None, "Anderson"),
        (None, "Anderson"),
        (None, "Anderson"),
        ("Jamie", None),
        ("Jamie", None),
        ("Jamie", None),
    )

    from os import environ
    from pprint import pprint

    conn = mariadb.connect(
        user=environ["MARIADB_USER"],
        password=environ["MARIADB_PASSWORD"],
        host="127.0.0.1",  # environ["MARIADB_HOST"],
        port=3306,
        database=environ["MARIADB_DATABASE"],
    )
    with conn.cursor() as cur:

        # for first_name, last_name in names_list:
        #     author = Author(first_name, last_name)
        #     author.create_in_dcc(conn)
        #     print(author.AuthorID)
        #     print(author)

        for FN, LN in names_list:
            usr = User(FN, LN)
            usr.commit(conn)

        cur.execute("SELECT AuthorID,FirstName,LastName FROM Author")
        for (id, first_name, last_name) in cur:
            print(f"ID: {id} First Name: {first_name}, Last Name: {last_name}")

    # from ldap3 import Server,Connection, ALL
    # >>> server = Server('ipa.demo1.freeipa.org', use_ssl=True, get_info=ALL)
    # >>> conn = Connection(server, 'uid=admin,cn=users,cn=accounts,dc=demo1,dc=freeipa,dc=org',
    #                       'Secret123', auto_bind=True)
    # >>> print(conn)
    # ldaps://ipa.demo1.freeipa.org:636 - ssl - user: uid=admin,cn=users,cn=accounts,dc=demo1,dc=freeipa,dc=org
    #           - bound - open - <local: 192.168.1.101:51438 - remote: 209.132.178.99:636> - tls not started -
    #           listening - SyncStrategy - internal decoder
    # ldap_uri = f""
    # server = Server(uri, use_ssl=True, get_info=ALL)
    # conn = Connection(server, auto_bind=True)
    from ldap3 import Server, Connection, ALL

    server = Server(environ["LDAP_HOST"], use_ssl=False, get_info=ALL)
    #   AuthLDAPURL ldap://ldap_server:389/ou=people,dc=ligo,dc=org
    #   AuthLDAPBindDN "cn=admin,dc=ligo,dc=org"
    #   AuthLDAPBindPassword "test1234"
    #   LDAP_HOST=ldap_server
    #   LDAP_ADMIN='admin'
    #   LDAP_ADMIN_PASSWORD=test1234
    #   LDAP_BASE_DN="dc=ligo,dc=org"
    #   LDAP_ORGANISATION="LIGO Make Believe"
    #   LDAP_DOMAIN="ligo.org"
    with Connection(
        server,
        f"cn={environ['LDAP_ADMIN']},{environ['LDAP_BASE_DN']}",
        environ["LDAP_ADMIN_PASSWORD"],
        auto_bind=True,
    ) as conn:
        search_base = f"ou=people,{environ['LDAP_BASE_DN']}"
        search_filter = "(uid=*)"
        attributes_to_return = (
            "uid",
            "givenName",
            "sn",
            "cn",
            "mail",
            "employeeNumber",
            "isMemberOf",
        )
        found = conn.search(search_base, search_filter, attributes=attributes_to_return)
        for entry in conn.entries:
            pprint(entry)
