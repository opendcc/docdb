from typing import Union

from models.igwn.User import User
from models.igwn.Users import Users
from models.igwn.HTTP import HTTPPost

from models.dcc.Document import Document
from models.dcc.DCCDB import DCCDB

class DocumentUploadToDCCFailed(Exception):
    pass

def create_document( document: Document,
                     root_url: str,
                     user : Union[str, User] = Users.get( ) ):
    if ( isinstance( user, str ) ):
        user = Users.get( user )
    post_request = HTTPPost(
        url = f'{root_url}/cgi-bin/private/DocDB/ProcessDocumentAdd',
        user = user
    )
    post_request.payload = {
        "mode": "use",
        "public": "",
        "oldnumber": document.doc_number,
        "requester": document.requester,
        "title": document.title,
        "abstract": document.abstract,
        "keywords": document.keywords,
        "doctype": document.doc_type_as_number( ),
        "security": document.security,
        "modify": document.modify,
        "authormanual": document.authors_as_string( ),
        "topics": document.topics,
    }
    post_request.commit( success_expression = \
                         f"You were successful. Your Document ID is <b>LIGO-{document.doc_number}</b>.",
                         exception = DocumentUploadToDCCFailed(
                             f"Upload of {document.doc_number} failed. \n"
                             "Most likely a document already exists with that number "
                         ) )


def create_user( cn: str = None ) -> User:
    """
    Create a new user

    The creation of a user requires multiple places to be updated.

    Attributes:
        cn (str): Common name for user

    Returns:
        User: Record holding user information.
    """
    # Create a new user in the collection of Users
    retval = Users.create_user( cn = cn )
    # Add the user to the DocDB system
    dcc_db = DCCDB( )
    dcc_db.connect( )
    dcc_db.create_user( retval )
    dcc_db.close_connection( )
    return( retval )
