import random

from models.igwn.MariaDB import MariaDB
from models.igwn.User import User

class DCCDB( MariaDB ):
    def create_user( self, User ):
        # ---------------------------------------------------------------
        # variables needed
        # ---------------------------------------------------------------
        common_name = ""
        email = ""
        employee_number = ""
        first_name = ""
        last_name = ""
        user_name = ""
        # ---------------------------------------------------------------
        # verify reasonable values
        # ---------------------------------------------------------------
        if ( hasattr( User, 'first_name' ) ):
            first_name = User.first_name
        else:
            first_name = Faker( ).first_name( )
            User.first_name = first_name
        if ( hasattr( User, 'last_name' ) ):
            last_name = User.last_name
        else:
            last_name = Faker( ).last_name( )
            User.last_name = last_name
        if ( hasattr( User, 'cn' ) ):
            common_name = User.cn
        else:
            common_name = f"{first_name} {last_name}"
            User.cn = common_name
        if ( hasattr( User, 'username' ) ):
            user_name = User.username
        else:
            user_name = f"{first_name}.{last_name}".lower( )
            User.username = user_name
        if ( hasattr( User, 'email' ) ):
            email = User.email
        else:
            email = f"{user_name}@local"
            User.email = email
        if ( hasattr( User, 'employee_number' ) ):
            employee_number = User.employee_number
        else:
            employee_number = random.randint(4001, 9999)
            User.employee_number = employee_number
        # ---------------------------------------------------------------
        # Update the database tables
        # ---------------------------------------------------------------
        query = \
            f"INSERT INTO Author SET Active=1, InstitutionID=26, " + \
            f" FirstName='{first_name}', " + \
            f" LastName='{last_name}'"
        print( f"{query}" )
        self.execute_query( query )
        query = \
            f"INSERT INTO EmailUser (Username, Name, EmailAddress, AuthorID, CanSign, EmployeeNumber) " + \
            f"  SELECT lower('{user_name}'), " + \
            f"          '{common_name}', " + \
            f"          lower('{email}'), " + \
            f"          AuthorID, 1, {employee_number} " + \
            f"  FROM Author " + \
            f"  WHERE LastName='{last_name}' and FirstName='{first_name}'"
        print( f"{query}" )
        self.execute_query( query )
        query = \
            f"INSERT INTO RemoteUser ( RemoteUserName, EmailAddress, EmailUserID) " + \
            f"     SELECT Username, EmailAddress, EmailUserID " + \
            f"     FROM EmailUser " + \
            f"     WHERE Username = lower('{user_name}')"
        print( f"{query}" )
        self.execute_query( query )

    def mark_user_active_state( self, user: User, active: bool = False ):
        """
        Mark the active state of the user

        Parameters:
            user(User): User to be updated. Must already exist and be unique.
            active(bool, optional): Active state \
                                   (true => Active, false => Inactive) \
                                   Defaults to False
        """
        query = \
            f"UPDATE Author" + \
            f" SET Active = {1 if active else 0}" + \
            f" WHERE LastName = '{user.last_name}' AND FirstName = 'user.first_name'"
        print( f"{query}" )
        self.execute_query( query )
