import pytest
import re
from os.path import exists
import os
import sys

from playwright.async_api import async_playwright

from models.igwn.UserContext import UserContext
from models.igwn.Users import Users

from models.DCC import DCC
from models.Events import *
from models.Login import Login
from models.RecentChanges import *
from models.ReserveNumber import *
from models.Search import *
from models.Topics import *

# put the more interesting stuff on top


class TestAsyncModel:
    from playwright.async_api import Page

    async def validate(self, page: Page, base_url: str) -> None:
        from playwright.async_api import expect

        # ---------------------------------------------------------------------
        # Ensure that auth is completed so other pages are accessable
        # ---------------------------------------------------------------------
        login_page = Login(page, base_url)
        await login_page.login(Users.get())

        dcc_page = DCC(page)
        await dcc_page.navigate()
        assert dcc_page.is_on_page()
        # ---------------------------------------------------------------------
        # Title
        # ---------------------------------------------------------------------
        assert await dcc_page.title.is_visible()

        # ---------------------------------------------------------------------
        #  Validation of title bar elements
        # ---------------------------------------------------------------------
        assert await dcc_page.home.is_visible()
        assert await dcc_page.reserve_number.is_visible()
        assert await dcc_page.search.is_visible()
        assert await dcc_page.recent_changes.is_visible()
        assert await dcc_page.topics.is_visible()
        assert await dcc_page.events.is_visible()
        assert await dcc_page.public.is_visible()
        assert await dcc_page.assist.is_visible()
        # print("other elements are visible")
        ## ---------------------------------------------------------------------
        ##  Validate navigation of title bar elements
        ## ---------------------------------------------------------------------
        await dcc_page.home.click()
        assert DCC(page).is_on_page()
        await dcc_page.reserve_number.click()
        assert ReserveNumber(page).is_on_page()
        await dcc_page.search.click()
        assert Search(page).is_on_page()
        await dcc_page.recent_changes.click()
        assert RecentChanges(page).is_on_page()
        await dcc_page.topics.click()
        assert Topics(page).is_on_page()
        await dcc_page.events.click()
        assert Events(page).is_on_page()
        ## ---------------------------------------------------------------------
        ##  Validation of buttons
        ## ---------------------------------------------------------------------
        await dcc_page.navigate()  # Return back to the main dcc page
        assert await dcc_page.author_button.is_visible()
        assert await dcc_page.title_button.is_visible()
        assert await dcc_page.identifier_button.is_visible()
        assert await dcc_page.changes_button.is_visible()
        assert await dcc_page.advanced_search_button.is_visible()
        assert await dcc_page.authors_button.is_visible()
        assert await dcc_page.topics_button.is_visible()
        assert await dcc_page.access_groups_button.is_visible()
        assert await dcc_page.events_by_type_button.is_visible()
        assert await dcc_page.events_by_date_button.is_visible()
        assert await dcc_page.signature_report_button.is_visible()
        # print("buttons are visible")
        assert await dcc_page.these_words_button.is_visible()

        ## ---------------------------------------------------------------------
        ## Footer
        ## ---------------------------------------------------------------------
        body = dcc_page.page.locator("body")
        await expect(
            body.get_by_text(
                "The LIGO Laboratory is supported by the "
                "National Science Foundation and operated "
                "jointly by Caltech and MIT. Any opinions, "
                "findings and conclusions or recommendations "
                "expressed in this material do not necessarily "
                "reflect the views of the "
                "National Science Foundation."
            )
        ).to_be_visible(timeout=2000)
        await expect(body).to_have_css("background-color", "rgb(221, 238, 255)")
        if "-dev" in base_url:
            dcc_logo = "dcc-dev-private-logo.png"
        else:
            dcc_logo = "dcc-private-logo.png"
        await expect(body).to_have_css(
            "background-image",
            'url("' + base_url + "/site-logos/" + dcc_logo + '"), '
            'url("' + base_url + '/site-logos/NSF_4-Color_bitmap_Logo.png")',
        )
        # ---------------------------------------------------------------------
        # Auto complete
        # ---------------------------------------------------------------------
        # :TODO: test author autocomplete on landing page

    @pytest.mark.asyncio
    async def test_01_DocumentDatabase_admin_async(
        self, global_initialize: None, browser_context_args, request, base_url
    ):
        async with async_playwright() as playwright:
            async with UserContext(
                request=request,
                playwright=playwright,
                browser_context_args=browser_context_args,
                base_url=base_url,
            ) as user1:
                await self.validate(user1.browser.page, base_url)


class TestSyncModel:
    from playwright.sync_api import Page

    def validate(self, page: Page, base_url: str) -> None:
        from playwright.sync_api import expect

        # ---------------------------------------------------------------------
        # Ensure that auth is completed so other pages are accessable
        # ---------------------------------------------------------------------
        login_page = Login(page, base_url)
        login_page.login(Users.get())

        dcc_page = DCC(page)
        dcc_page.navigate()
        assert dcc_page.is_on_page()
        # ---------------------------------------------------------------------
        # Title
        # ---------------------------------------------------------------------
        assert dcc_page.title.is_visible()

        # ---------------------------------------------------------------------
        #  Validation of title bar elements
        # ---------------------------------------------------------------------
        assert dcc_page.home.is_visible()
        assert dcc_page.reserve_number.is_visible()
        assert dcc_page.search.is_visible()
        assert dcc_page.recent_changes.is_visible()
        assert dcc_page.topics.is_visible()
        assert dcc_page.events.is_visible()
        assert dcc_page.public.is_visible()
        assert dcc_page.assist.is_visible()
        # print("other elements are visible")
        # ---------------------------------------------------------------------
        #  Validate navigation of title bar elements
        # ---------------------------------------------------------------------
        dcc_page.home.click()
        assert DCC(page).is_on_page()
        dcc_page.reserve_number.click()
        assert ReserveNumber(page).is_on_page()
        dcc_page.search.click()
        assert Search(page).is_on_page()
        dcc_page.recent_changes.click()
        assert RecentChanges(page).is_on_page()
        dcc_page.topics.click()
        assert Topics(page).is_on_page()
        dcc_page.events.click()
        assert Events(page).is_on_page()
        # ---------------------------------------------------------------------
        #  Validation of buttons
        # ---------------------------------------------------------------------
        dcc_page.navigate()  # Return back to the main dcc page
        assert dcc_page.author_button.is_visible()
        assert dcc_page.title_button.is_visible()
        assert dcc_page.identifier_button.is_visible()
        assert dcc_page.changes_button.is_visible()
        assert dcc_page.advanced_search_button.is_visible()
        assert dcc_page.authors_button.is_visible()
        assert dcc_page.topics_button.is_visible()
        assert dcc_page.access_groups_button.is_visible()
        assert dcc_page.events_by_type_button.is_visible()
        assert dcc_page.events_by_date_button.is_visible()
        assert dcc_page.signature_report_button.is_visible()
        # print("buttons are visible")
        assert dcc_page.these_words_button.is_visible()

        # ---------------------------------------------------------------------
        # Footer
        # ---------------------------------------------------------------------
        body = dcc_page.page.locator("body")
        expect(
            body.get_by_text(
                "The LIGO Laboratory is supported by the "
                "National Science Foundation and operated "
                "jointly by Caltech and MIT. Any opinions, "
                "findings and conclusions or recommendations "
                "expressed in this material do not necessarily "
                "reflect the views of the "
                "National Science Foundation."
            )
        ).to_be_visible(timeout=2000)
        expect(body).to_have_css("background-color", "rgb(221, 238, 255)")
        if "-dev" in base_url:
            dcc_logo = "dcc-dev-private-logo.png"
        else:
            dcc_logo = "dcc-private-logo.png"
        expect(body).to_have_css(
            "background-image",
            'url("' + base_url + "/site-logos/" + dcc_logo + '"), '
            'url("' + base_url + '/site-logos/NSF_4-Color_bitmap_Logo.png")',
        )
        # ---------------------------------------------------------------------
        # Auto complete
        # ---------------------------------------------------------------------
        # :TODO: test author autocomplete on landing page

    def test_01_DocumentDatabase_admin_sync(
        self, global_initialize: None, page: Page, base_url
    ):
        self.validate(page, base_url)
