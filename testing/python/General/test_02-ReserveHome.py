import re

from playwright.sync_api import Page, expect

from models.Login import *
from models.ReserveNumber import *


def test_02_ReserveHome(global_initialize: None, page: Page, base_url):
    # ---------------------------------------------------------------------
    # Ensure that auth is completed so other pages are accessible
    # ---------------------------------------------------------------------
    Login(page, base_url).login(Users.get( ))

    reserve_page = ReserveNumber(page)
    reserve_page.navigate()
    assert reserve_page.is_on_page()
    # ---------------------------------------------------------------------
    # Title
    # ---------------------------------------------------------------------
    assert reserve_page.title.is_visible()
    # ---------------------------------------------------------------------
    #  Validation of buttons
    # ---------------------------------------------------------------------
    assert reserve_page.basic_search_button.is_visible()
    assert reserve_page.number_search_button.is_visible()
    assert reserve_page.use_button.is_visible()
    assert reserve_page.reserve_button.is_visible()
