"""
Ticket: https://git.ligo.org/DCC/docdb/-/issues/453

Title: public document has info about associated events which aren't public

I happened to notice that on https://dcc.ligo.org/LIGO-P1400033/public, the associated
events (in this case, LVC meetings) are partly revealed to public viewers: not the name
of the event, but the date and location.  This seems like a bug.  (Not a big deal since
there's no sensitive information in that, but it probably looks puzzling to public
viewers.)

"""

import pytest

from faker import Faker
from playwright.sync_api import Page, expect

class Test_453:
    """
    Scenario:
    """
    def test_private_event_reference_in_public_document( self, page: Page, base_url: str ):
        pass
