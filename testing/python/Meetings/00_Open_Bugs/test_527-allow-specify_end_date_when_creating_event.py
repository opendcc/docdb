"""
Ticket: https://git.ligo.org/DCC/docdb/-/issues/527

Title: Allow user to specify event end date when creating an event

DCC events can have a time range -- a start date and and end date -- but the way to set
the end date is obscure. As a result, many DCC events just get created with a start date
even if they are really multi-day
(e.g. see https://dcc.ligo.org/cgi-bin/private/DocDB/ListAllMeetings?eventgroupid=2).

Would it be possible to modify the "Create a New Event" form to allow the user to
create a multi-day event to start with?
"""
import pytest

from faker import Faker
from playwright.sync_api import Page, expect

class Test_527:
    def test_allow_specify_end_date_when_creating_event( self, page: Page, base_url: str ):
        """
        Scenario:
        """
        pass
