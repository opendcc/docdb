import pytest

from faker import Faker
from playwright.sync_api import Page, expect

from models.igwn.Users import Users

from models.Login import Login
from models.SessionModify import SessionModify
from models.ShowCalendar import ShowCalendar

"""
Scenario: Author autocomplete widget is available and functional
"""
class Test_01_singleday_event:
    """
    Scenario: Checks the calendar for a link to create an event

    """
    def test_ChooseDateOfUSAIndependence( self, page: Page, base_url: str ):
        # ---------------------------------------------------------------
        # Open the page as the admin user
        # ---------------------------------------------------------------
        login_page = Login( page, base_url )
        login_page.login( Users.get( ) )

        calendar = ShowCalendar( page )
        calendar.DateURL( month = 7,
                          day = 4,
                          year = 1776 )
        calendar.navigate( )


    """
    Scenario: Creates an event for GW150914
        Given: A clean system
            And: New Session for for 9/14/2015
        Actions:
            Select: event group 'other'
            Fill: 

    """
    def test_CreateEventForGW150914( self, page: Page, base_url: str ):
        # ---------------------------------------------------------------
        # Open the page as the admin user
        # ---------------------------------------------------------------
        login_page = Login( page, base_url )
        login_page.login( Users.get( ) )

        # Create the GW150914 event
        event = SessionModify( page )
        event.NewSessionURL( singlesession = 1,
                             day = 14,
                             month = 9,
                             year = 2015 )
        event.navigate( )
        event.SelectEventGroup( "other" )
        event.description_short.fill( 'GW150914' )
        event.description_long.fill( 'First direct detection of gravitational waves' )
        event.location.fill( 'LLO and LHO' )
        event.url.fill( 'https://dcc.ligo.org/P150914/public' )
        event.session_description.fill( 'we did it !!!' )
        event.list_of_moderators.fill( '' )
        event.list_of_moderators.press_sequentially( 'On' )
        event.user_suggestions.click( )
        event.list_of_moderators.press_sequentially( 'Thr' )
        event.user_suggestions.click( )
        event.button_create_event_and_talks.click( )
        # Check the outcome of creating the event
        event.created_new_event( "GW150914" ).is_visible( )
        event.display_the_event.is_visible( )
        event.instructions.is_visible( )
        # Add talks to the event
        event.talk_title.first.fill( 'First Talk' )
        event.talk_title.last.fill( 'Last Talk' )
        event.talk_title.nth( 1 ).fill( 'Last Talk' )
        event.button_modify_event_and_talks.click( )
        event.added_item_to_agenda_first_talk.is_visible( )
