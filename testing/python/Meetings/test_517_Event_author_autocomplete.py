import pytest
import re

from faker import Faker
from playwright.sync_api import Page, expect

from models.igwn.Users import Users

from models.ListAllMeetings import ListAllMeetings
from models.ListBy import ListBy
from models.Login import Login
from models.MeetingsModify import MeetingsModify
from models.SessionModify import SessionModify
from models.ShowCalendar import ShowCalendar

"""
Scenario: Author autocomplete widget is available and functional
"""
class Test_517_Check_Author_autocomplete_widgets_is_available_and_functional:
    """
    Scenario: Single day event
    """
    def test_SingleDayEvent(self, page: Page, base_url: str ):
        # ---------------------------------------------------------------
        # Open the page as the admin user
        # ---------------------------------------------------------------
        login_page = Login( page, base_url )
        login_page.login( Users.get( ) )

        # ---------------------------------------------------------------
        # ---------------------------------------------------------------
        fake = Faker( )
        SingleDayEventName = 'Autocomplete ' + \
            str( fake.random_number( digits=8, fix_len=True ) )
        calendar = ShowCalendar( page )
        calendar.navigate( )
        calendar.today.click( )
        # Test: Creates a new event and assigns a moderator using autocomplete
        session = SessionModify( page )
        session.create_new_event.click( )
        session.SelectEventGroup( 'other' )
        session.description_short.fill( SingleDayEventName )
        session.description_long.fill( 'Test autocomplete for Modos and Presenters' )
        session.location.fill(  'LLO and LHO' )
        session.session_description.fill( 'we did it !!!' )
        session.topics.select_option( 'Data Analysis' )
        session.list_of_moderators.fill( '' )
        session.list_of_moderators.press_sequentially( 'On' )
        session.user_suggestions.click()
        session.list_of_moderators.press_sequentially( 'Thre' )
        session.user_suggestions.click()
        expect( session.user_suggestions ).not_to_be_visible( )
        expect( session.list_of_moderators ).to_have_value( re.compile( r'^Three, User$', re.MULTILINE ) )
        expect( session.list_of_moderators ).to_have_value( re.compile( r'^One, User$', re.MULTILINE ) )
        session.button_create_event_and_talks.click( )

        # Check the outcome of creating the event
        expect( session.created_new_event_autocomplete ).to_be_visible()
        expect( session.display_the_event ).to_be_visible( )
        expect( session.instructions ).to_be_visible( )

        # Test: Creates a session and uses autocomplete to assign the presenter
        session.talk_title.first.type('First Talk')
        session.list_authors_n1.fill( '' )
        session.list_authors_n1.press_sequentially( 'Tw' )
        session.user_suggestions.click()
        session.button_modify_event_and_talks.click()

        # Check the outcome of creating the session
        expect( session.added_item_to_agenda_first_talk ).to_be_visible()

        # Test: Checks the event is visible on the event list
        all_meetings = ListAllMeetings( page )
        all_meetings.navigate( )
        all_meetings.page.evaluate("window.scrollTo(0, document.body.scrollHeight)")
        all_meetings.button_ligo_number( SingleDayEventName ).click( )

    """
    Scenario: Multi day event
    """
    def test_MultiDayEvent(self, page: Page, base_url: str ):
        # ---------------------------------------------------------------
        # Open the page as the admin user
        # ---------------------------------------------------------------
        login_page = Login( page, base_url )
        login_page.login( Users.get( ) )

        # ---------------------------------------------------------------
        # ---------------------------------------------------------------
        fake = Faker( )
        MultiDayEventName = 'Multiday ' + \
            str( fake.random_number( digits=8, fix_len=True ) )
        # Test: Checks Author autocomplete widgets are available and functional in Multi-Day Events
        # Test: Creates a multi-day event with moderators' names by autocomplete
        meeting = MeetingsModify( page )
        meeting.navigate( )
        meeting.event_groups.select_option( 'Reviews' )
        meeting.day_start.select_option( '5' )
        meeting.day_end.select_option( '12' )
        meeting.description_short.fill( MultiDayEventName )
        meeting.description_long.fill( 'Test autocomplete for Modos and Presenters on multiday events' )
        meeting.list_of_moderators.fill ( '' )
        meeting.list_of_moderators.press_sequentially( 'On' )
        meeting.popup_user_suggestions.click( )
        meeting.button_modify_event_and_sessions.click( )
        expect( meeting.description_created_event ).to_be_visible( )

        # Test: Adds a new session to the event and adds session moderator using autocomplete
        meeting.session_title.fill( 'Session #1' )
        meeting.list_of_moderators_n1.fill( '' )
        meeting.list_of_moderators_n1.press_sequentially( 'Tw' )
        meeting.popup_user_suggestions.click()
        meeting.button_modify_event_and_sessions.click( )
        expect( meeting.description_modified_event ).to_be_visible( )
        expect( meeting.description_created_session ).to_be_visible( )

        # Test: Checks the event is visible on the event list
        all_meetings = ListAllMeetings( page )
        all_meetings.navigate( )
        all_meetings.button_ligo_number( MultiDayEventName ).click( )
