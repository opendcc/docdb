import pytest  # only needed to skip, safe to remove otherwise
from playwright.sync_api import Page, expect

# import relevant page/document modedl
from models.Login import *
from models.ReserveNumber import *


"""
extremely basic and barebones template to helpcreate new integration tests
filename needs to start by test_ in order to be taken into account in pytest

functions whose name starts with test_ will be automatically discovered by pytest
current recommendation is one integration test per file, and the name of the function matches the name of the file
obviously one can create supporting functions to make test writing and understanding better.
we'll see how this pans out
"""


def test_bug_000(global_initialize: None, page: Page, base_url):
    # ---------------------------------------------------------------------
    # Ensure that auth is completed so other pages are accessible
    # ---------------------------------------------------------------------
    Login(page, base_url).login(Users.get( ))

    # ---------------------------------------------------------------------
    #
    # ---------------------------------------------------------------------

    # ---------------------------------------------------------------------
    #
    # ---------------------------------------------------------------------


class TestSuiteNumber1:
    def setup_method(self):
        print(
            "sTestSuiteNumber1.setup_method(); seems you cannot use or setup fixtures here"
        )

    def teardown_method(self):
        # tear down self.attribute
        print("TestSuiteNumber1.teardown_method()")

    def test_something(
        self,
        global_initialize: None,
        page: Page,
        base_url,
        document_with_permission,
    ):
        pass


pytest.skip(
    "this is only an educational example meant as a template, skipping.",
    allow_module_level=True,
)
