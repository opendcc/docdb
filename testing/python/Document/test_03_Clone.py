from playwright.sync_api import Page, expect

# import relevant page/document model
from models.igwn.Users import Users

from models.dcc.Document import Document
from models.dcc.Utilities import create_document

from models.Login import Login
from models.ShowDocument import ShowDocument
from models.XMLClone import XMLClone


class Test03Clone:
    def test_simple(
            # fixtures
            global_initialize: None,
            page: Page,
            base_url: str,
            random_document: Document ):
        """
        Scenario: Simple cloning of the document

        Given:    A random document created by admin user
         When:    The admin user chooses "Clone Document" from document page
          And:      accepting the dialog warning
         Then:    The page should contain the text "You were successful."
          And:      the button "Go To New Document" should be visible
          And:      clicking on the button should bring up the cloned document.
        """
        login_page = Login( page, base_url )
        login_page.login( Users.get( "admin") )

        document_page = ShowDocument( page )
        document_page.navigate( random_document.doc_number )
        document_page.clone_button.click( )
        xmlclone_page = XMLClone( document_page.page )
        expect( xmlclone_page.successful ).to_be_visible( )
        xmlclone_page.goto_new_document_button.click( )

    def test_bug_542(
            # fixtures
            global_initialize: None,
            page: Page,
            base_url: str ):
        """
        Scenario: Clone a document with problematic content (ascii control codes)

        Given:    A document with ascii control codes in its title
          And:      with ascii control codes in its abstract
          And:      created by admin user
         When:    The admin user chooses "Clone Document" from document page
          And:      accepting the dialog warning
         Then:    The page should contain the text "You were successful."
          And:      the button "Go To New Document" should be visible
          And:      clicking on the button should bring up the cloned document.
        """
        login_page = Login( page, base_url )
        login_page.login( Users.get( "admin") )

        document = Document( )
        document.title = "Title\n with \v line \ffeed"
        document.abstract = "\b abstract with backspace"
        create_document( document, base_url )

        document_page = ShowDocument( page )
        document_page.navigate( document.doc_number )
        document_page.clone_button.click( )
        xmlclone_page = XMLClone( document_page.page )
        expect( xmlclone_page.successful ).to_be_visible( )
        xmlclone_page.goto_new_document_button.click( )


