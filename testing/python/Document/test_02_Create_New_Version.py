from playwright.sync_api import Page
from playwright.sync_api import expect

# import relevant page/document model
from models.Login import *
from models.DocumentAddForm import *


def test_02_Create_New_Version(
    global_initialize: None, page: Page, base_url, random_document, file_to_upload
):
    # ---------------------------------------------------------------------
    # Ensure that auth is completed so other pages are accessible
    # ---------------------------------------------------------------------
    Login(page, base_url).login(Users.get( ))

    # ---------------------------------------------------------------------
    # create a document version x0 in DCC,
    # then open the page to upload a v1
    # ---------------------------------------------------------------------
    master_doc = random_document  # <- fixture in tests/fixtures.py
    new_version_page = DocumentAddForm(page)
    new_version_page.navigate_to_upload_version(master_doc.document_number)

    # ---------------------------------------------------------------------
    # check we have fields that have not been checked elsewhere
    # ---------------------------------------------------------------------
    expect(new_version_page.revision_note).to_be_visible()
    expect(new_version_page.local_file_upload_block).to_be_visible()
    expect(new_version_page.filedesc1_input).to_be_visible()
    expect(new_version_page.upload1_file_chooser).to_be_visible()
    expect(new_version_page.main1_checkbox).to_be_visible()

    assert new_version_page.upload_new_version_buttons.count() == 4

    # ---------------------------------------------------------------------
    # change the number of dynamic upload slots
    # and verifiy the number of rows in tbale matches
    # ---------------------------------------------------------------------
    # TODO: test dynaslots

    # ---------------------------------------------------------------------
    # add a file in the first slot
    # ---------------------------------------------------------------------
    new_version_page.filedesc1_input.fill("description of file 1")
    new_version_page.upload1_file_chooser.set_input_files(file_to_upload)
    new_version_page.upload_new_version_buttons.first.click()

    # ---------------------------------------------------------------------
    # verify success
    # ---------------------------------------------------------------------
    assert new_version_page.upload_successful_message.is_visible()
    assert new_version_page.view_document_button.is_visible()
    assert not new_version_page.error_message.is_visible()
    new_version_page.view_document_button.click()
