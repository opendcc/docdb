import re

from playwright.sync_api import Page, expect

from models.Login import *
from models.DocumentAddForm import *


# PEP8: retain prevailing culture even though there is no backwrd compatibility to worry about ?
# DocumentAddForm vs document_add_form ?
# I'd say (and write) the former because it matches the name of the perl script actually under scrutiny
def test_01_DocumentAddForm(global_initialize: None, page: Page, base_url):
    # ---------------------------------------------------------------------
    # Ensure that auth is completed so other pages are accessible
    # ---------------------------------------------------------------------
    Login(page, base_url).login(Users.get( ))

    # ---------------------------------------------------------------------
    # Ensure that auth is completed so other pages are accessible
    # ---------------------------------------------------------------------
    document_add_page = DocumentAddForm(page)
    document_add_page.navigate("reserve")

    # ---------------------------------------------------------------------
    #  Validation of form fields and buttons
    # ---------------------------------------------------------------------
    assert not document_add_page.old_dcc_number.is_visible()  # or .count() == 0
    assert document_add_page.title.is_visible()
    assert document_add_page.abstract.is_visible()
    assert document_add_page.keywords.is_visible()
    assert document_add_page.doctype.is_visible()
    assert document_add_page.view_hierarchical.is_visible()
    assert document_add_page.view_specific.is_visible()
    assert document_add_page.modify_hierarchical.is_visible()
    assert document_add_page.modify_specific.is_visible()
    assert document_add_page.authors.is_visible()
    assert document_add_page.topics.count() == 4
    assert document_add_page.reserve_buttons.count() == 2
    assert document_add_page.use_dcc_number_buttons.count() == 0
    assert document_add_page.submit_document_buttons.count() == 0
    # ---------------------------------------------------------------------
    # Fill in the form
    # ---------------------------------------------------------------------
    document_add_page.title.fill("First Doc")
    document_add_page.abstract.fill(
        "this is my abstract, FIXME: add some tex expression to test that later"
    )
    document_add_page.keywords.fill("cypress dcc automation test temporary")
    document_add_page.doctype.check()
    document_add_page.view_hierarchical.select_option(label="LIGO_Lab")
    document_add_page.modify_specific.select_option(label="Authors")
    document_add_page.authors.fill("Istrator, Admin")
    # document_add_page.topics.first.select_option(label='  Temporary Test Document')
    # using the value instead of the label here b/c of non asci &nbsp; characters in the label
    # using a substring comes out empty, using a regex throws an exception ...
    document_add_page.topics.first.select_option(value="59")
    # all is ready , let's submit
    document_add_page.reserve_buttons.first.click()

    # ---------------------------------------------------------------------
    # Check for success
    # ---------------------------------------------------------------------

    assert document_add_page.successful_message.is_visible()


def test_01_DocumentAddForm_old_dcc_number(
    global_initialize: None, page: Page, base_url
):
    """
    Similar to the page to reserve a document number, but with the option to enter a custom document alias: GW150914
    :param global_initialize:
    :param page:
    :param base_url:
    :return:
    """
    # ---------------------------------------------------------------------
    # Ensure that auth is completed so other pages are accessible
    # ---------------------------------------------------------------------
    Login(page, base_url).login(Users.get( ))

    # ---------------------------------------------------------------------
    # Ensure that auth is completed so other pages are accessible
    # ---------------------------------------------------------------------
    use_oldcc_number_page = DocumentAddForm(page)
    use_oldcc_number_page.navigate("use")

    # ---------------------------------------------------------------------
    #  Validation of form fields and buttons
    # ---------------------------------------------------------------------
    assert use_oldcc_number_page.old_dcc_number.is_visible()
    assert use_oldcc_number_page.title.is_visible()
    assert use_oldcc_number_page.abstract.is_visible()
    assert use_oldcc_number_page.keywords.is_visible()
    assert use_oldcc_number_page.doctype.is_visible()
    assert use_oldcc_number_page.view_hierarchical.is_visible()
    assert use_oldcc_number_page.view_specific.is_visible()
    assert use_oldcc_number_page.modify_hierarchical.is_visible()
    assert use_oldcc_number_page.modify_specific.is_visible()
    assert use_oldcc_number_page.authors.is_visible()
    assert use_oldcc_number_page.topics.count() == 4
    assert use_oldcc_number_page.reserve_buttons.count() == 0
    assert use_oldcc_number_page.use_dcc_number_buttons.count() == 2
    assert use_oldcc_number_page.submit_document_buttons.count() == 0


def test_01_DocumentAddForm_add(global_initialize: None, page: Page, base_url):
    # ---------------------------------------------------------------------
    # Ensure that auth is completed so other pages are accessible
    # ---------------------------------------------------------------------
    Login(page, base_url).login(Users.get( ))

    # ---------------------------------------------------------------------
    # Ensure that auth is completed so other pages are accessible
    # ---------------------------------------------------------------------
    document_add_page = DocumentAddForm(page)
    document_add_page.navigate("add")

    # ---------------------------------------------------------------------
    #  Validation of form fields and buttons
    # ---------------------------------------------------------------------
    assert not document_add_page.old_dcc_number.is_visible()  # or .count() == 0
    assert document_add_page.title.is_visible()
    assert document_add_page.abstract.is_visible()
    assert document_add_page.keywords.is_visible()
    assert not document_add_page.doctype.is_visible()
    assert document_add_page.view_hierarchical.is_visible()
    assert document_add_page.view_specific.is_visible()
    assert document_add_page.modify_hierarchical.is_visible()
    assert document_add_page.modify_specific.is_visible()
    assert document_add_page.authors.is_visible()
    assert document_add_page.topics.count() == 4
    assert document_add_page.reserve_buttons.count() == 0
    assert document_add_page.use_dcc_number_buttons.count() == 0
    assert document_add_page.submit_document_buttons.count() == 4
