import pytest  # only needed to skip, safe to remove otherwise
from playwright.sync_api import Page, expect
from os import environ

# import relevant page/document modedl
from models.Login import *
from models.DocumentAddForm import *
from models.ShowDocument import *
from models.dcc.MailAPI import MailHog, MailMessage


# ---------------------------------------------------------------------
#
# ---------------------------------------------------------------------


class TestMetaDataAndSignature:
    """
    Scenario1: Create a Serial Signature
        Given: a document without a signature block
        When: I create a "serial signature" with the "change metadata" button
        Then: I can see the signature block on the Document Card
            And: the first signer can see the 'sign', 'deny' and 'abstain' buttons
            And: the subsequent signers cannot see the 'sign', 'deny', 'abstain' nor 'unsign' buttons
            And: the final approver can see the 'approve' and 'disapprove' buttons
            And: exactly one notification email sent only to the first signer

    Scenario2: Checks the document signature features
        Given: a document without a signature block
        When: I create a "parallel signature" with the "change metadata" button
        Then: I can see the signature block on the Document Card
            And: exactly 1 (?) email has been sent to notify that the document is ready for signature
            And: recipient list of the email is made of all the signatories except for the final approver

    Scenario3: Number of Documents Pending Signature is Correct
        Given: a document with a parallel signature
            And: one of the signatories does not have permission to read the document (user3)
        When: each signer visits the /dcc url
            And: they visit the signature report pages
        Then: the number of document pending signature on /dcc is equal to the number of documents displayed
              in the signature report page in the first 2 tables "waiting for signature" and "waiting for approval"
    """

    def setup_method(self):
        pass

    def teardown_method(self):
        # tear down self.attribute
        print("MetaDataAndSignature.teardown_method()")

    def test_01_create_serial_signature(
        self,
        global_initialize: None,
        page: Page,
        base_url,
        random_document,
    ):
        """
        Scenario1: Create a Serial Signature
            Given: a document without a signature block
            When: I create a "serial signature" with the "change metadata" button
            Then: I can see the signature block on the Document Card
                And: the first signer can see the 'sign', 'deny' and 'abstain' buttons
                And: the subsequent signers cannot see the 'sign', 'deny', 'abstain' nor 'unsign' buttons
                And: the final approver can see the 'approve' and 'disapprove' buttons
                And: exactly one notification email sent only to the first signer
        """
        assert global_initialize is None

        # ---------------------------------------------------------------------
        # Ensure that auth is completed so other pages are accessible
        # ---------------------------------------------------------------------
        login_page = Login(page, base_url)
        login_page.login(Users.get( ))

        document_number = random_document.document_number
        metadata_page = DocumentAddForm(page)
        metadata_page.change_metadata(document_number)

        metadata_page.signature_text_area.fill("User One\nUser Two\nUser Five")
        metadata_page.parallel_signoff_checkbox.uncheck()
        metadata_page.change_metadata_button.last.click()

        # TODO: implement '''And: exactly one notification email with only to the first signer'''

        document_card_page = ShowDocument(page)

        login_page.login(Users.get( "user1" ))
        document_card_page.navigate(document_number)
        expect(document_card_page.access_denied_message).not_to_be_visible()
        expect(document_card_page.sign_button).to_be_visible()
        expect(document_card_page.deny_button).to_be_visible()
        expect(document_card_page.abstain_button).to_be_visible()
        document_card_page.sign_button.click()
        # expect(document_card_page.unsign_button).to_be_visible()
        # document_card_page.unsign_button.click()  # otherwise the test below does not make a lot of sense

        login_page.login(Users.get( "user2" ))
        document_card_page.navigate(document_number)
        expect(document_card_page.access_denied_message).not_to_be_visible()
        expect(document_card_page.sign_button).not_to_be_visible()
        expect(document_card_page.deny_button).not_to_be_visible()
        expect(document_card_page.abstain_button).not_to_be_visible()
        expect(document_card_page.unsign_button).not_to_be_visible()

        login_page.login(Users.get( "user5" ))
        document_card_page.navigate(document_number)
        expect(document_card_page.access_denied_message).not_to_be_visible()
        expect(document_card_page.sign_button).not_to_be_visible()
        expect(document_card_page.deny_button).not_to_be_visible()
        expect(document_card_page.abstain_button).not_to_be_visible()
        expect(document_card_page.approve_button).to_be_visible()
        expect(document_card_page.disapprove_button).to_be_visible()
        document_card_page.approve_button.click()
        # expect(document_card_page.unapprove_button).to_be_visible()

    def test_02_create_parallel_signature(
        self,
        global_initialize: None,
        page: Page,
        base_url,
        random_document,
    ):
        """
        Scenario2: Checks the document signature features
        Given: a document without a signature block
        When: I create a "parallel signature" with the "change metadata" button
        Then: I can see the signature block on the Document Card
            And: exactly 1 (?) email has been sent to notify that the document is ready for signature
            And: recipient list of the email is made of all the signatories except for the final approver
        """
        assert global_initialize is None

        # ---------------------------------------------------------------------
        # Ensure that auth is completed so other pages are accessible
        # ---------------------------------------------------------------------
        Login(page, base_url).login(Users.get( ))

        document_number = random_document.document_number
        metadata_page = DocumentAddForm(page)
        metadata_page.change_metadata(document_number)

        metadata_page.signature_text_area.fill("User One\nUser Two\nUser Five")
        metadata_page.parallel_signoff_checkbox.check()
        metadata_page.change_metadata_button.last.click()

        document_card_page = ShowDocument(page)
        document_card_page.navigate(document_number)
        assert document_card_page.signoff_block.is_visible()
        assert document_card_page.waiting_signature_message.count() > 0
        assert document_card_page.waiting_approval_message.count() == 1
        assert document_card_page.waiting_approval_message.is_visible()

        # TODO: maybe make the mail server url a fixture ?
        resp = MailMessage.fromMailServer(apiurl=environ["MAILHOG_URL"]).contains(
            term=document_number
        )

        assert resp.contains(kind="recipient", term=Users.get("user1").username).count() == 1
        assert resp.contains(kind="recipient", term=Users.get("user2").username).count() == 1
        assert resp.contains(kind="recipient", term=Users.get("user5").username).count() == 0

    def test_03_documents_pending_signature(
        self,
        global_initialize: None,
        page: Page,
        base_url,
        random_document,
    ):
        """
        Scenario3: Number of Documents Pending Signature is Correct
            Given: a document with a parallel signature
                And: one of the signatories does not have permission to read the document (user3)
            When: each signer visits the /dcc url
                And: they visit the signature report pages
            Then: the number of document pending signature on /dcc is equal to the number of documents displayed
                  in the signature report page in the first 2 tables "waiting for signature" and "waiting for approval"
        """
        assert global_initialize is None

        # ---------------------------------------------------------------------
        # Ensure that auth is completed so other pages are accessible
        # ---------------------------------------------------------------------
        login_page = Login(page, base_url)
        login_page.login(Users.get( ))

        document_number = random_document.document_number
        metadata_page = DocumentAddForm(page)
        metadata_page.change_metadata(document_number)
        metadata_page.signature_text_area.fill("User One\nUser Three\nUser Five")
        metadata_page.parallel_signoff_checkbox.check()
        metadata_page.change_metadata_button.last.click()

        document_card_page = ShowDocument(page)
        #
        #  it('checks correct number of pending signature is displayed', () => {
        #    var documents_pending_signature_before;
        #    var documents_pending_signature_after;
        #    cy.visit('/dcc', {
        #                auth: {
        #                    username: 'user1@local',
        #                    password: 'password',
        #                },
        #            })
        #    cy.get('strong').contains(/You have \d+ documents? to sign/).then( (elm) => {
        #        documents_pending_signature_before= parseInt(elm.get(0).innerText.split(" ")[3])
        #        cy.wrap(documents_pending_signature_before).as('signatures_before')
        #        cy.log(documents_pending_signature_before)
        #    })
        #
        #    // create a few docs with signature requirements e.g. 5
        #    // on #1 gather all signatures
        #    // on #2 gather some signature and expect it pending
        #    // on #3 gather some signature, have a final (dis)approval and expect it not pending
        #    // #4 and #5 leave as is
        #    // check the DocumentDatabase page and expect 3 documents pending signature (probably useful to check a delta
        #    // from beginning of test rather than absolute number: you don't want to assume to know the initial state)
        #    cy.visit('/dcc', {
        #        auth: {
        #            username: 'user1@local',
        #            password: 'password',
        #        },
        #    })
        #    cy.get('strong').contains(/You have \d+ documents? to sign/).then( (elm) => {
        #        documents_pending_signature_after= parseInt(elm.get(0).innerText.split(" ")[3])
        #        cy.log(documents_pending_signature_after)
        #        cy.wrap(documents_pending_signature_after).as('signatures_aftatures_before').then((signatures_before) =>{
        #                                                                                 cy.get('@signatures_after').then((signatures_after')
        #    })
        #    cy.get('@sign) => {
        #            cy.log("New signature(s)"+ (signatures_after-signatures_after))
        #        })
        #    })
        #
        #    // open the signature report and see documents #1,2,3,4 and 5 in the expected table
        # 
        # })
