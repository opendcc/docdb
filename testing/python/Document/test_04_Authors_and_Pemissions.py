import pytest  # only needed to skip, safe to remove otherwise
from playwright.sync_api import Page, BrowserContext, expect

# import DCC helper classes
from models.dcc.Document import Document
from models.dcc.Utilities import create_document

# import relevant page/document model
from models.Login import *
from models.ShowDocument import *


class TestAuthorsAndPermissions:
    def test_author_access(
        self,
        global_initialize: None,
        page: Page,
        base_url,
    ):

        # ---------------------------------------------------------------------
        # Given a M type document in DCC with 2 authors : admin and user2
        # ---------------------------------------------------------------------
        mdoc = Document( )
        mdoc.authors = ["Two, User", "Istrator, Admin"]
        mdoc.doc_type = 'M'
        mdoc.doc_number_prefix( 2 )
        create_document( mdoc, base_url )
        print(f"given document number: {mdoc.doc_number}")

        # ---------------------------------------------------------------------
        #  Then user1 should have access because they are lvk member
        # ---------------------------------------------------------------------
        login_page = Login(page, base_url)
        login_page.login(Users.get( "user1" ))
        document_card_page = ShowDocument(page)
        document_card_page.navigate(mdoc.doc_number)
        expect(document_card_page.abstract).to_be_visible()
        expect(document_card_page.authors).to_be_visible()
        expect(document_card_page.modifiable).to_be_visible()
        expect(document_card_page.viewable).to_be_visible()
        expect(document_card_page.access_denied_message).not_to_be_visible()

        # ---------------------------------------------------------------------
        #  And Then user2 should have access because it's an author
        # ---------------------------------------------------------------------
        login_page.login(Users.get( "user2" ))
        document_card_page.navigate(mdoc.doc_number)
        expect(document_card_page.abstract).to_be_visible()
        expect(document_card_page.authors).to_be_visible()
        expect(document_card_page.modifiable).to_be_visible()
        expect(document_card_page.viewable).to_be_visible()
        expect(document_card_page.access_denied_message).not_to_be_visible()

        # ---------------------------------------------------------------------
        #  And Then user3 and user4 should not have access
        # ---------------------------------------------------------------------
        login_page.login(Users.get( "user3" ))
        document_card_page.navigate(mdoc.doc_number)
        expect(document_card_page.access_denied_message).to_be_visible()
        expect(document_card_page.abstract).not_to_be_visible()
        expect(document_card_page.authors).not_to_be_visible()
        expect(document_card_page.modifiable).not_to_be_visible()
        expect(document_card_page.viewable).not_to_be_visible()

        login_page.login(Users.get( "user4" ))
        document_card_page.navigate(mdoc.doc_number)
        expect(document_card_page.access_denied_message).to_be_visible()
        expect(document_card_page.abstract).not_to_be_visible()
        expect(document_card_page.authors).not_to_be_visible()
        expect(document_card_page.modifiable).not_to_be_visible()
        expect(document_card_page.viewable).not_to_be_visible()
