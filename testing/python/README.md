The current directory needs to be added to the
PYTHONPATH so the local modules can be located.

```
export PYTHONPATH=.
```

Running the tests under pytest interactively

```
pytest --slowmo 1000 --base-url https://dcc-dev.ligo.org --video on --output ~/tmp/test-results --headed --template=html1/index.html --report=${HOME}/tmp/report.html
```


Running all tests on all browsers

```

pytest General  --browser webkit --browser firefox --browser chromium --base-url https://dcc
```

Running a specific test

```
pytest --base-url https://dcc \
  --output ~/tmp/test-results \
  --template=html1/index.html \
  --report=${HOME}/tmp/report.html \
  -k test_01
```
Running two specific test

```
pytest --base-url https://dcc \
  --output ~/tmp/test-results \
  --template=html1/index.html \
  --report=${HOME}/tmp/report.html \
  -k 'test_01 or test_02'
```

Running a test from a specific directory

```
 pytest General/test_03-Clone.py  --browser firefox --base-url https://dcc
```

## Developing of new tests

When developing new tests, it is best to use 

### Setup 

Create pytest.ini to establish defaults so as to reduce what
needs to be passed on the command line.

```
[pytest]
base_url = https://dcc

[playwright]
output = /e2e/playwright/test-results
screenshot = only-on-failure
video = on
tracing = retain-on-failure
```

Setup for batch mode

```
export IGWN_WEBAPP_TESTING_MODE=batch
```

Establish a python virtual environment

```
python -m venv dev_testing
pip install -r requirements.txt
```

Load 

### Run new test
- fully automated
```
pytest Document/test_01_DocumentAddForm.py 
```
- automated with visual feedback (proved useful debugging issues with locators)
```
pytest -s --headed --browser firefox --slowmo 1000 Document/test_01_DocumentAddForm.py 
```


### BDD with pytest. can it leverage playwright ?

https://pypi.org/project/pytest-bdd/




###  Posting to an API

```python 
    browser.new_context(base_url="https://api.github.com")
    api_request_context = context.request
    page = context.new_page()

    # Alternatively you can create a APIRequestContext manually without having a browser context attached:
    # api_request_context = p.request.new_context(base_url="https://api.github.com")

    # Create a repository.
    response = api_request_context.post(
        "/user/repos",
        headers={
            "Accept": "application/vnd.github.v3+json",
            # Add GitHub personal access token.
            "Authorization": f"token {API_TOKEN}",
        },
        data={"name": REPO},
    )
```
