import pytest
from playwright.sync_api import Page, expect

from models.dcc.Document import Document
from models.dcc.Utilities import create_document, create_user

from models.DocumentAddForm import DocumentAddForm
from models.Login import Login
from models.ShowDocument import ShowDocument

class TestBugzilla516:
    class Parameters:
        def __init__( self, browser_context_args, request, page: Page, base_url: str ):
            self.browser_context_args = browser_context_args
            self.request = request
            self.page = page
            self.base_url = base_url

    def test_create_document_with_parallel_signoff(
            self,
            browser_context_args,
            request,
            page: Page,
            base_url: str ):
        parameters = self.Parameters( browser_context_args, request, page, base_url )
        self.verify( parameters )

    def verify( self, parameters: Parameters ):
        #----------------------------------------------------------------
        # Create random user who is inactive
        #----------------------------------------------------------------
        inactive_user = create_user( )
        #----------------------------------------------------------------
        # Sign in as the admin user
        #----------------------------------------------------------------
        login_page = Login( parameters.page, parameters.base_url )
        login_page.login( "admin" )

        #----------------------------------------------------------------
        # Create random document as the admin user
        #----------------------------------------------------------------
        document = Document(
            doc_type = "M",
            prefix = 1 )
        create_document( document = document,
                         root_url = parameters.base_url,
                         user = "admin" )

        #     # Generate fake user data
        #     user = self.create_fake_user()

        # Visit the document page as admin
        # and create a parallel signoff with a temporary user
        change_metadata_page = ShowDocument( parameters.page )
        change_metadata_page.navigate( document.document_number )
        change_metadata_page.change_metadata_button.first.click( )
        change_metadata_page = DocumentAddForm( parameters.page )
        change_metadata_page.signature_text_area.press_sequentially( "User Two" )
        change_metadata_page.page.keyboard.press( 'Escape' )
        change_metadata_page.page.keyboard.press( 'Enter' )
        change_metadata_page.signature_text_area.press_sequentially(
            f"{inactive_user.cn}" )
        change_metadata_page.page.keyboard.press( 'Escape' )
        change_metadata_page.page.keyboard.press( 'Enter' )
        change_metadata_page.signature_text_area.press_sequentially( "User Three" )
        change_metadata_page.page.keyboard.press( 'Escape' )
        change_metadata_page.page.keyboard.press( 'Enter' )
        change_metadata_page.parallel_signoff_checkbox.check( )
        change_metadata_page.change_metadata_button.first.click( )
        change_metadata_page.view_document_button.first.click( )

    # def test_users_sign_then_update_author_list(self):
    #     pytest.skip("Not implemented yet")

    # def test_signature_list_remains_unchanged(self):
    #     pytest.skip("Not implemented yet")
