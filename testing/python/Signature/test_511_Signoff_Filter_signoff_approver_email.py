import pytest  # only needed to skip, safe to remove otherwise
from playwright.sync_api import Page, expect

# import relevant page/document modedl
from models.Login import *
from models.ReserveNumber import *


"""
TODO: Write test Scenarios implemented here
Scenario1: 
    Given: a document with no signature 
    When: a signature (parallel ? serial ? does not matter) is requested with 6 or more signers 
    Then: every signer should receive an individualized email letting them know about it
        And: the final approver should not receive the notification email.
        
Scenario2:
    Given: a document with a parallel signature with 6 or more signers
    When: one of the intermediate signer acts on their signature to sign, deny or abstain
        And: the final approver and an other signers are still pending
    Then: no notification email about the signature is sent

Scenario3:
    Given: a document with a serial signature with 6 or more signers
    When: the pending signer acts on their signature to sign, deny or abstain
        And: the final approver still hasn't acted in their signature
    Then: a notification email is sent to the next signer only
    
Scenario4:
    Given: a document with a parallel signature with 6 or more signers
    When: the last intermediate signer acts on their signature to sign, deny or abstain
        And: the final approver is still pending
    Then: the final approver should receive a notification email announcing that the
          document is ready for approval
        
Scenario5:
    Given: a document with a parallel signature with 6 or more signers
    When: the final approver acts on their signature to approve or disapprove
    Then: 
        What type of notification should (or not) happen
        
Scenario6:
   Given: a document with a parallel signature with 6 or more signers
        And: at least one intermediate signers has signed or denied 
        And: the document is still waiting for final approval
    When: one of the intermediate signers clears their selection
    Then: 
          What type of notification should (or not) happen
"""


class test_511_Filter_Signoff_Approver_Email:
    def setup_method(self):
        print(
            "sTestSuiteNumber1.setup_method(); seems you cannot use or setup fixtures here"
        )

    def teardown_method(self):
        # tear down self.attribute
        print("TestSuiteNumber1.teardown_method()")

    def test_something(
        self,
        global_initialize: None,
        page: Page,
        base_url,
        document_with_permission,
    ):
        pass

pytest.skip(
    "2023-08-01: this module is not fully implemented yet and "
    "the code that it verifies is not fully available yet.",
    allow_module_level=True,
)
