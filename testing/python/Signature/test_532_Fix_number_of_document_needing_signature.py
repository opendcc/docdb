import pytest

from playwright.async_api import Page, expect

# import relevant page/document model
from models.igwn.Mob import Mob
from models.igwn.UserContext import UserContext
from models.igwn.Users import Users

from models.dcc.Document import Document
from models.dcc.Utilities import create_document

from models.DCC import DCC
from models.DocumentAddForm import DocumentAddForm
from models.Login import Login
from models.ShowDocument import ShowDocument

@pytest.mark.usefixtures("base_url")  # Assuming you have a fixture defined for the base URL
class TestPendingSignatures:
    class Parameters:
        def __init__( self, browser_context_args, request, page: Page, base_url: str ):
            self.browser_context_args = browser_context_args
            self.request = request
            self.page = page
            self.base_url = base_url

    async def signature_count( self, parameters: Parameters, user_context: UserContext ):
        dcc_page = DCC( user_context.browser.page )
        signatures = await dcc_page.page.locator('[cy-data="SignatureReportForm"]').text_content( )
        return( signatures )

    async def verify_pending_signatures( self, parameters: Parameters, mob: Mob ):
        #----------------------------------------------------------------
        # Get the number of signatures before
        #----------------------------------------------------------------
        signatures_before = await self.signature_count( parameters, mob.context( 'user1' ) )
        print( f'Signatures before: {signatures_before}' )

        #----------------------------------------------------------------
        # Generate fake document data
        #----------------------------------------------------------------
        document = Document(
            doc_type = "M",
            prefix = 1,
        )
        create_document( document = document,
                         root_url = parameters.base_url,
                         user = Users.get( "admin" ) )

        #----------------------------------------------------------------
        # Create document with user as non - final signer
        #----------------------------------------------------------------
        change_metadata_page = ShowDocument( mob.context( "admin" ).browser.page )
        await change_metadata_page.navigate( document.document_number )
        await change_metadata_page.change_metadata_button.first.click( )

        change_metadata_page = DocumentAddForm( mob.context( "admin" ).browser.page )
        await change_metadata_page.signature_text_area.fill( 'User One\nAdministrator\n' )
        await change_metadata_page.parallel_signoff_checkbox.check( )
        await change_metadata_page.change_metadata_button.first.click( )
        await change_metadata_page.view_document_button.first.click( )

        #----------------------------------------------------------------
        # Sign as final approver
        #----------------------------------------------------------------
        # change_metadata_page.page.locator('button.pda2bl:visible').click( )
        # change_metadata_page.page.locator('input.approver:visible').click( )

        #----------------------------------------------------------------
        # Verify the number of pending signatures has not changed
        #----------------------------------------------------------------
        # Get the number of signatures after
        signatures_after = await self.signature_count( parameters, mob.context( 'user1' ) )
        print( f'Signatures after: {signatures_after}')
        assert signatures_before == signatures_after

    async def verify( self, parameters: Parameters ):
        import time  # TODO: remove me ...
        #----------------------------------------------------------------
        # Create a random document where user1 is signer, admin approver
        #----------------------------------------------------------------
        async with Mob( UserNames = [ "admin", "user1" ],
                        BrowserContextArgs = parameters.browser_context_args,
                        Request = parameters.request,
                        Page = parameters.page,
                        BaseURL = parameters.base_url,
                        login_class = Login ) as mob:
            await self.verify_pending_signatures( parameters, mob )

    @pytest.mark.asyncio
    async def test_pending_signatures(
            self, browser_context_args, request,
            page: Page, base_url: str ):
        """
        Scenario: Verify that the number of signatures does not change when document is approved
           Given: A document requiring signatures from two authors
            When: The final approver (admin) approves the document
             And: The other author (user1) has not signed off
            Then: The number of signatures required of user1 will not have changed relative to the
                  number of signatures required of user1 before the creation of the document
        """
        parameters = self.Parameters( browser_context_args, request, page, base_url )
        await self.verify( parameters )
