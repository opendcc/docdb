import pytest

from playwright.async_api import async_playwright
from playwright.async_api import Page, expect

from models.dcc.Document import Document
from models.dcc.User import User
from models.dcc.DCCDB import DCCDB
from models.dcc.Utilities import create_document, create_user

from models.DocumentAddForm import DocumentAddForm
from models.Login import Login
from models.ShowDocument import ShowDocument

class TestDocumentSignature:
    class Parameters:
        def __init__( self, browser_context_args, request, page: Page, base_url: str ):
            self.browser_context_args = browser_context_args
            self.request = request
            self.page = page
            self.base_url = base_url

    def create_document_and_mark_inactive( self, parameters: Parameters  ):
        parameters.document = Document(
            doc_type = "M",
            prefix = 1 )
        create_document( document = parameters.document,
                         root_url = parameters.base_url,
                         user = "admin" )


    def create_inactive_user( self, parameters ):
        parameters.inactive_user = create_user( )

    def test_remove_inactive_user_from_signature(
            self,
            browser_context_args,
            request,
            page: Page,
            base_url: str ):
        """
        Scenario:
           Given: A document
             And: Needs to be signed by User Two
             And: Needs to be signed by <random_inactive_user>
             And: Needs to be signed by User Three
            When: <random_inactive_user> becomes inactive
            Then: The message 'At least one of the accounts in the signoff list is inactive'
                  should not be visible
             And: The message 'waiting for other signatures' should appear to the right of
                  <random_inactive_user>
        """
        parameters = self.Parameters( browser_context_args, request, page, base_url )
        #----------------------------------------------------------------
        # Sign in as the admin user
        #----------------------------------------------------------------
        login_page = Login( parameters.page, parameters.base_url )
        login_page.login( "admin" )

        self.create_document_and_mark_inactive( parameters )
        self.create_inactive_user( parameters )

        # Your test code using Playwright goes here
        change_metadata_page = ShowDocument( parameters.page )
        change_metadata_page.navigate( parameters.document.document_number )
        change_metadata_page.change_metadata_button.click( )
        change_metadata_page = DocumentAddForm( parameters.page )
        change_metadata_page.signature_text_area.fill(
            f'User Two\n' +
            f'{parameters.inactive_user.first_name}' +
            f' {parameters.inactive_user.last_name}\n' +
            f'User Three\n' )
        change_metadata_page.change_metadata_button.first.click( )
        verification_text = "At least one of the accounts in the signoff list is inactive."
        #----------------------------------------------------------------
        # assert "At least one of the accounts in the signoff list is inactive." not in page.content()
        #----------------------------------------------------------------
        signature_page = ShowDocument( parameters.page )
        signature_page.navigate( parameters.document.document_number )
        # expect( signature_page.signoff_info ).not_to_contain_text( verification_text )
        # expect( signature_page.page.content( ) ).not_to_contain_text( "waiting for other signatures" )
        #----------------------------------------------------------------
        # Deactivate the user
        #----------------------------------------------------------------
        dcc_database = DCCDB( )
        dcc_database.connect( )
        dcc_database.mark_user_active_state( user = parameters.inactive_user, active = False )
        dcc_database.close_connection( )
        signature_page.navigate( parameters.document.document_number )
        expected_signoffs = 3
        #assert len( signature_page.signoff_info.all( ) ) == expected_signoffs, \
        #    f"There are {len(signature_page.signoff_info.all( ))} signoff info entries instead of {expected_signoffs}
        assert len( signature_page.signoff_info.all( ) ) ==  expected_signoffs, \
            f"There are {len(signature_page.signoff_info.all( ))} signoff info entries" + \
            f" instead of {expected_signoffs}"
        # expect( signature_page.signoff_info ).not_to_contain_text( verification_text )
        # expect( signature_page.signoff_info ).to_contain_text( "waiting for other signatures" )

    def test_remove_inactive_user_from_final_signature(
            self,
            browser_context_args,
            request,
            page: Page,
            base_url: str ):
        """
        Scenario:
           Given: A document
             And: Needs to be signed by User Two
             And: Needs to be signed by <random_inactive_user>
             And: Needs to be signed by User Three
            When: <random_inactive_user> becomes inactive
            Then: The message 'At least one of the accounts in the signoff list is inactive'
                  should not be visible
             And: The message 'waiting for other signatures' should appear to the right of
                  <random_inactive_user>
        """
        parameters = self.Parameters( browser_context_args, request, page, base_url )
        #----------------------------------------------------------------
        # Sign in as the admin user
        #----------------------------------------------------------------
        login_page = Login( parameters.page, parameters.base_url )
        login_page.login( "admin" )

        self.create_document_and_mark_inactive( parameters )
        self.create_inactive_user( parameters )

        # Your test code using Playwright goes here
        change_metadata_page = ShowDocument( parameters.page )
        change_metadata_page.navigate( parameters.document.document_number )
        change_metadata_page.change_metadata_button.click( )
        change_metadata_page.signature_text_area.press_sequentially( f'User Two' )
        change_metadata_page.page.keyboard.press( 'Escape' )
        change_metadata_page.page.keyboard.press( 'Enter' )
        change_metadata_page.signature_text_area.press_sequentially( f'User Three' )
        change_metadata_page.page.keyboard.press( 'Escape' )
        change_metadata_page.page.keyboard.press( 'Enter' )
        change_metadata_page.signature_text_area.press_sequentially(
            f'{parameters.inactive_user.first_name}' +
            f' {parameters.inactive_user.last_name}' )
        change_metadata_page.page.keyboard.press( 'Escape' )
        change_metadata_page.page.keyboard.press( 'Enter' )
        change_metadata_page.change_metadata_button.first.click( )
        #----------------------------------------------------------------
        # assert "At least one of the accounts in the signoff list is inactive." not in page.content()
        #----------------------------------------------------------------
        verification_text = "At least one of the accounts in the signoff list is inactive."
        signature_page = ShowDocument( parameters.page )
        signature_page.navigate( parameters.document.document_number )
        # expect( signature_page.page.content( ) ).not_to_contain_text( verification_text )
        # expect( signature_page.page.content( ) ).not_to_contain_text( "waiting for other signatures" )
        #----------------------------------------------------------------
        # Deactivate the user
        #----------------------------------------------------------------
        dcc_database = DCCDB( )
        dcc_database.connect( )
        dcc_database.mark_user_active_state( user = parameters.inactive_user, active = False )
        dcc_database.close_connection( )
        signature_page.navigate( parameters.document.document_number )
        expected_signoffs = 3
        assert len( signature_page.signoff_info.all( ) ) == expected_signoffs, \
            f"There are {len(signature_page.signoff_info.all( ))} signoff info entries instead of {expected_signoffs}"
        # expect( signature_page.page.content( ) ).not_to_contain_text( verification_text )
        # expect( signature_page.page.content( ) ).to_contain_text( "waiting for other signatures" )
