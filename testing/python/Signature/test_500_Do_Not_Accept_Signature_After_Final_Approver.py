import pytest

from playwright.async_api import async_playwright
from playwright.async_api import Page, expect

from models.igwn.Mob import Mob
from models.igwn.Users import Users

from models.Login import Login
from models.DocumentAddForm import DocumentAddForm
from models.ShowDocument import ShowDocument


"""
Scenario: "Approve" button displays a denial message if the Final Approver has already signed
    Given: a document with a parallel signature with user1, user2 as signers, admin as approver
      And: user1 can see the "Approve", "Deny" and "Abstain" button on the document card
      And: admin approves the document
     When: user1 clicks on "Approve" button
     Then: user1 sees a denial message
      And: user1's page refreshes the signature block automatically after 5 to 10 seconds
     Then: user1's signature block does not show any signature action button
      And: user1's signature block displays the final approver timestamp for the closure of the process.

Repeat Scenario above for "Deny" and "Abstain" buttons
"""


async def confirmApproveDocument(dialog):
    print(dialog.message)
    await dialog.accept()


class Test_500_Signature_After_Approver:
    async def verify( self, action : str = "" ):
        import time  # TODO: remove me ...

        # ---------------------------------------------------------------------
        # Create a random document where user1 is signer, admin approver
        # ---------------------------------------------------------------------
        async with Mob( UserNames = [ "admin", "user1" ],
                        BrowserContextArgs = self.browser_context_args,
                        Request = self.request,
                        Page = self.page,
                        BaseURL = self.base_url,
                        login_class = Login ) as mob:
            #------------------------------------------------------------
            # Document number
            #------------------------------------------------------------
            document_number = self.random_document.document_number

            #------------------------------------------------------------
            # Setup document for parallel signoff
            #------------------------------------------------------------
            admin_page = DocumentAddForm(mob.context('admin').browser.page)
            await admin_page.change_metadata(document_number)
            await admin_page.signature_text_area.fill(
                "User One\nUser Two\nAdministrator"
            )
            await admin_page.parallel_signoff_checkbox.check()
            await admin_page.change_metadata_button.last.click()

            # ---------------------------------------------------------------------
            # user1 opens the document card and verifies the "Sign" button is accessible
            # ---------------------------------------------------------------------

            user1_page = ShowDocument(mob.context('user1').browser.page)
            await user1_page.navigate(document_number)
            await expect(user1_page.sign_button).to_be_visible()
            await expect(user1_page.abstain_button).to_be_visible()
            await expect(user1_page.deny_button).to_be_visible()

            # ---------------------------------------------------------------------
            # Meanwhile admin decides to approve the document
            # ---------------------------------------------------------------------
            admin2_page = ShowDocument(mob.context('admin').browser.page)
            await admin2_page.navigate(document_number)
            admin2_page.page.on("dialog", confirmApproveDocument)
            await expect(admin2_page.approve_button).to_be_visible()
            await expect(admin2_page.disapprove_button).to_be_visible()
            await expect(admin2_page.unapprove_button).not_to_be_visible()
            await admin2_page.approve_button.click()
            await expect(admin2_page.approve_button).not_to_be_visible()

            # ---------------------------------------------------------------------
            # user1 decides to sign and discovers they can't because the process is closed
            # ---------------------------------------------------------------------
            # user1's document card is unchanged by admin's actions in a different browser
            await expect(user1_page.sign_button).to_be_visible()
            await expect(user1_page.abstain_button).to_be_visible()
            await expect(user1_page.deny_button).to_be_visible()
            await expect(user1_page.approve_button).not_to_be_visible()
            await expect(user1_page.unapprove_button).not_to_be_visible()
            await expect(user1_page.disapprove_button).not_to_be_visible()
            #------------------------------------------------------------
            # Actions to be taken by usr1 after admin has approved
            #------------------------------------------------------------
            if ( action == "sign" ):
                await user1_page.sign_button.click()
            elif ( action == "deny" ):
                await user1_page.deny_button.click()
            elif ( action == 'abstain' ):
                await user1_page.abstain_button.click()
            #------------------------------------------------------------
            await expect(user1_page.signature_warning_message).to_be_visible()
            # message display for up to 10 sec hence the 15000ms timeout below (10 + some extra)
            await expect(user1_page.page.get_by_text("Approved on")).to_be_visible(
                timeout=15000
            )
            await expect(user1_page.sign_button).not_to_be_visible(timeout=15000)
            await expect(user1_page.unsign_button).not_to_be_visible(timeout=15000)
            await expect(user1_page.deny_button).not_to_be_visible(timeout=15000)
            await expect(user1_page.abstain_button).not_to_be_visible(timeout=15000)

            time.sleep( 10 )

    @pytest.mark.asyncio
    async def test_user_cannot_sign_after_approver(
        self, browser_context_args, request, page: Page, base_url, random_document
    ):
        self.browser_context_args = browser_context_args
        self.request = request
        self.page = page
        self.base_url = base_url
        self.random_document = random_document
        await self.verify( "sign" )

    @pytest.mark.asyncio
    async def test_user_cannot_deny_after_approver(
        self, browser_context_args, request, page: Page, base_url, random_document
    ):
        self.browser_context_args = browser_context_args
        self.request = request
        self.page = page
        self.base_url = base_url
        self.random_document = random_document
        await self.verify( "deny" )

    @pytest.mark.asyncio
    async def test_user_cannot_abstain_after_approver(
        self, browser_context_args, request, page: Page, base_url, random_document
    ):
        self.browser_context_args = browser_context_args
        self.request = request
        self.page = page
        self.base_url = base_url
        self.random_document = random_document
        await self.verify( "abstain" )

if __name__ == "__main__":
    from pprint import pprint

    pprint(Users.get("user1").__dict__)
