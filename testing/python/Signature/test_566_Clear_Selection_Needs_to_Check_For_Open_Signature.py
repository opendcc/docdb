import pytest

from playwright.async_api import async_playwright
from playwright.async_api import Page, expect

from models.igwn.UserContext import UserContext
from models.igwn.Users import Users

from models.Login import Login
from models.DocumentAddForm import DocumentAddForm
from models.ShowDocument import ShowDocument

"""
Scenario: "Clear Selection" button displays a denial message if the Final Approver has already signed
    Given: a document with a parallel signature with user1, user2 as signers, admin as approver
      And: user1 has already signed and can see the "clear selection" button on the document card  
      And: admin approves the document 
     When: user1 clicks on the "clear selection" button
     Then: user1 sees a denial message
      And: user1's selection is not cleared
      And: the page should refresh the signature block automatically after 5 to 10 seconds
     Then: the signature block must not show any signature action button 
      And: the signature block must display the final approver timestamp for the closure of the process.
"""


async def confirmApproveDocument(dialog):
    print(dialog.message)
    await dialog.accept()


@pytest.mark.asyncio
async def test_user_cannot_unsign_after_approver(
    browser_context_args, request, page: Page, base_url, random_document
):
    # ---------------------------------------------------------------------
    # Create a random document where user1 and user2 are signers, admin approver
    # ---------------------------------------------------------------------
    async with async_playwright() as playwright:
        async with UserContext(
            request=request,
            playwright=playwright,
            browser_context_args=browser_context_args,
            base_url=base_url,
        ) as admin_context:
            admin_context_page = admin_context.browser.page
            login_page = Login(admin_context_page, base_url)
            await login_page.login(Users.get("admin"))
            document_number = random_document.document_number

            admin_page = DocumentAddForm(admin_context_page)
            await admin_page.change_metadata(document_number)
            await admin_page.signature_text_area.fill(
                "User One\nUser Two\nAdministrator"
            )
            await admin_page.parallel_signoff_checkbox.check()
            await admin_page.change_metadata_button.last.click()

        # ---------------------------------------------------------------------
        # user1 opens the document card and verifies the "Sign" button is accessible
        # ---------------------------------------------------------------------
        async with UserContext(
            request=request,
            playwright=playwright,
            browser_context_args=browser_context_args,
            base_url=base_url,
        ) as user1_context:
            user1_context_page = user1_context.browser.page
            login_page = Login(user1_context_page, base_url)
            await login_page.login(Users.get("user1"))
            user1_page = ShowDocument(user1_context_page)
            await user1_page.navigate(document_number)
            await expect(user1_page.sign_button).to_be_visible()
            await expect(user1_page.abstain_button).to_be_visible()
            await expect(user1_page.deny_button).to_be_visible()
            # ---------------------------------------------------------------------
            # user1 signs the document
            # ---------------------------------------------------------------------
            await user1_page.sign_button.click()
            await expect(user1_page.signature_warning_message).not_to_be_visible()
            await expect(user1_page.sign_button).not_to_be_visible()
            await expect(user1_page.unsign_button).to_be_visible()

            # ---------------------------------------------------------------------
            # Meanwhile admin decides to approve the document
            # ---------------------------------------------------------------------
            async with UserContext(
                request=request,
                playwright=playwright,
                browser_context_args=browser_context_args,
                base_url=base_url,
            ) as admin2_context:
                admin2_context_page = admin2_context.browser.page
                login_page = Login(admin2_context_page, base_url)
                await login_page.login(Users.get("admin"))
                admin2_page = ShowDocument(admin2_context_page)
                await admin2_page.navigate(document_number)
                admin2_page.page.on("dialog", confirmApproveDocument)
                await expect(admin2_page.approve_button).to_be_visible()
                await expect(admin2_page.disapprove_button).to_be_visible()
                await expect(admin2_page.unapprove_button).not_to_be_visible()
                await admin2_page.approve_button.click()
                await expect(admin2_page.approve_button).not_to_be_visible()

            # ---------------------------------------------------------------------
            # user1 decides to sign and discovers they can't because the process is closed
            # ---------------------------------------------------------------------
            # user1's document card is unchanged by admin's actions in a different browser
            await expect(user1_page.sign_button).not_to_be_visible()
            await expect(user1_page.abstain_button).not_to_be_visible()
            await expect(user1_page.deny_button).not_to_be_visible()
            await expect(user1_page.approve_button).not_to_be_visible()
            await expect(user1_page.unapprove_button).not_to_be_visible()
            await expect(user1_page.disapprove_button).not_to_be_visible()
            await expect(user1_page.unsign_button).to_be_visible()
            await user1_page.unsign_button.click()
            await expect(user1_page.signature_warning_message).to_be_visible()
            # message display for up to 10 sec hence the 15000ms timeout below (10 + some extra)
            await expect(user1_page.page.get_by_text("Approved on")).to_be_visible(
                timeout=15000
            )
            await expect(user1_page.sign_button).not_to_be_visible(timeout=15000)
            await expect(user1_page.unsign_button).not_to_be_visible(timeout=15000)
            await expect(user1_page.deny_button).not_to_be_visible(timeout=15000)
            await expect(user1_page.abstain_button).not_to_be_visible(timeout=15000)
