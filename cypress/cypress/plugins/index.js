/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

// load the environment variables from the local .env file
require('dotenv').config()

const mysql = require("mysql");
const ldap = require('ldapjs');

function queryTestDb(query, config) {
  // creates a new mysql connection using credentials from cypress.json env's

  const connection = mysql.createConnection({
    host     : config.env['MARIADB_HOST'],
    user     : config.env['MARIADB_USER'],
    password : config.env['MARIADB_PASSWORD'],
    database : config.env['MARIADB_DATABASE']
  });
  // start connection to db
  connection.connect();
  // exec query + disconnect to db as a Promise
  return new Promise((resolve, reject) => {
    connection.query(query, (error, results) => {
      // should not silence error...
      if (error) reject(error);
      else {
        connection.end();
        // console.log(results)
        return resolve(results);
      }
    });
  });
}

function insertLDAP(ldif, config) {
/*
    dn: employeeNumber=1000,ou=people,dc=ligo,dc=org
    cn: developer
    employeenumber: 1000
    employeetype: Engineer
    gidnumber: 1000
    givenname: Philippe
    homedirectory: /home/developer
    objectclass: inetOrgPerson
    objectclass: organizationalPerson
    objectclass: person
    objectclass: posixAccount
    objectclass: top
    objectclass: eduMember
    objectclass: eduPerson
    ou: people
    sn: Developer
    ismemberof: Communities:LVC:LIGOLab:DCC:DCCTesters
    isMemberOf: Communities:LSCVirgoLIGOGroupMembers
    ismemberof: gw-astronomy:KAGRA-LIGO:members
    uid: developer
    uidnumber: 41000
    userpassword: password
*/

//    const client = ldap.createClient({
//      url: ['ldap://127.0.0.1:1389', 'ldap://127.0.0.2:1389']
//    });
//
//    client.on('error', (err) => {
//      // handle connection error
//      alert(err)
//    });
//
//    const entry = {
//      cn: 'foo',
//      sn: 'bar',
//      email: ['foo@bar.com', 'foo1@bar.com'],
//      objectclass: 'fooPerson'
//    };
//    client.add('cn=foo, o=example', entry, (err) => {
//      assert.ifError(err);
//    });
    console.log('not implemented yet !');
    return null;
}


/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars
module.exports = (on, config) => {
  // Usage: cy.task('queryDb', query)
  on("task", {
      queryDb: query => {
          return queryTestDb(query, config);
//      },
//      addLDAP: ldif => {
//          return insertLDAP(ldif, config);
      }
  });
};
