describe('Signoff: Filter signoff approver\'s emails: Bugzilla #511', () => {
    // it is a template really as I ma not sure what the test scenario is other than
    // create  a doc with signature
    // sign in certain way
    // check that mailhog contains the expected emails no more, no less
    const faker = require('faker')
    const masterDocumentNumber = 'M'+(100000+faker.datatype.number({'nbDigits': 8, 'strict':true}));
    // document and user created here to but visible inside multiple it() if needed.
    // random document
    const document = {
        Number: 'M'+(100000+faker.datatype.number({'nbDigits': 8, 'strict':true})),
        requester: 3,
        Title: faker.lorem.sentence(),
        Abstract: faker.lorem.paragraph(),
        doctype: 7,
        authors:  "One, User\nIstrator, Admin",
    };
    // Add Random user just to add them to the signature
    let user = {
        first: faker.name.firstName().replace("'","_") ,
        last: faker.name.lastName().replace("'","_") ,
    };
    it('creates '+document.Number+' with multiple signers in parallel', () => {
         cy.api_createUser(user)

         cy.api_createDocument(document)
         cy.visit('/LIGO-'+document.Number+'-x0', {
             auth: {
                 username: 'admin',
                 password: 'admin',
             },
         })
         // create a parallel signoff with our temporary user
         cy.get("input[type=submit]").contains("Change Metadata").click()
         // {esc} to remove name completion
         cy.get("#signofflist").type("User Two{esc}{enter}User One{esc}{enter}Administrator{esc}{enter}")
         cy.get("input[name=parallelsignoff]").check()
         cy.get("input[type=submit]").contains("Change Metadata").last().click()
         cy.get("button.pda2bl").contains("View Document").should('be.visible')
   })

    it('signs and unsigns '+document.Number, () => {
        cy.visit('/LIGO-'+document.Number+'-x0', {
            auth: {
                 username: 'user1@local',
                 password: 'password',
            },
        })
        cy.get('[data-cy="SignButton"]').should('be.visible').click()
    })

    it('verifies the proper number of emails in the approver\'s mailbox', () =>{
        cy.mhGetMailsBySubject('Ready for signature: LIGO-'+document.Number+'-x0: '+document.Title)
        .should('have.length', 1);
    })
})