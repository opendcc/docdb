// Hopefully, one day, these are really going to be DocDB api calls...
Cypress.Commands.add('api_createDocument', document => {
    // document={
    //    Number: "M345210",
    //    requester: 3,
    //    Title: "test doc",
    //    Abstract: "Abstract bla, bla",
    //    authors: "Author 1\nAuhtor 2\n"
    // }
    cy.visit({
          url: "/cgi-bin/private/DocDB/ProcessDocumentAdd",
          method: "POST",
          auth: {
            username: 'admin',
            password: 'admin',
          },
          body: {
              oldnumber: document.Number,
              mode: "use",
              public: "",
              requester:  document.requester,
              title: document.Title,
              abstract: document.Abstract,
              keywords: "cypress POST automation test document ",
              doctype: document.doctype,
              security: "2",
              modify: "2",
              authormanual: document.authors,
              topics: 59,
          }
    })
});

Cypress.Commands.add('api_createUser', user => {
    user.employeeNumber = Cypress._.random(4001, 9999);
    cy.queryDb("INSERT INTO Author SET Active=1, InstitutionID=26, "+
                                     " FirstName= '"+ user.first+"', "+
                                     " LastName= '"+ user.last+"' ;");
    //  insert into EmailUser set Username= 'pierce.klein', Name = 'Pierce Klein', EmailAddress = 'pierce.klein@local', AuthorID=10;
    cy.queryDb("INSERT INTO EmailUser (Username, Name, EmailAddress, AuthorID, CanSign, EmployeeNumber) "+
               "  SELECT lower(concat(FirstName,'.',LastName)), "+
               "          concat(FirstName,' ', LastName), "+
               "          lower(concat(FirstName,'.',LastName, '@local')), "+
               "          AuthorID, 1, "+ user.employeeNumber +" " +
               "  FROM Author "+
               "  WHERE LastName='"+user.last+"' and FirstName='"+user.first+"'; ");
    // Matching RemoteUser
    cy.queryDb("INSERT INTO RemoteUser ( RemoteUserName, EmailAddress, EmailUserID) "+
               "     SELECT Username, EmailAddress, EmailUserID "+
               "     FROM EmailUser "+
               "     WHERE Username = '"+user.first+"."+user.last+"';");
    // FIXME: default group membership ?

    // FIXME: add in LDAP
    // cy.addLDAP(user);
});

